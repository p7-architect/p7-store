# p7-store

## 软件架构
* 架构概念图
  <img src="doc/架构概念图.png">

* 架构技术选型图
  <img src="doc/架构技术选型.png">


## 组件版本及工具类说明

--------------

| 组件 | 版本 | 说明 | 重要等级(数字越低优先级越高)  |
|:----|:----:|:----:|:----:|
| Jdk | 8 |  |        1         |
| Spring Boot | 2.5.8| |        1         |
|Grpc| 1.45.1 | Rpc框架 |        1         |
|Istio| 1.5.9 | 管理服务间通信 |        1         |
| Mysql | 8.0.29 |  关系型数据库  |        1         |
| K8s | 1.19.16 |  容器编排管理  |        1         |
| Docker | 20.10.15 |  容器化部署  |        1         |
| Hutool-all |  5.8.2 |  工具类  |        1         |
| Druid-spring-boot |  1.2.9 |  数据库连接池  |        1         |
| Spring-cloud |  2021.0.3 |    |        1         |
| Guava |  31.1-jre |  工具类  |        1         |
| Mybatis-plus-boot-starter |  3.5.1 |  mybatis插件  |        1         |
| Mapstruct |  1.4.2.Final |  对象转换工具类  |        1         |
| Redis | 6.2.7 |  NoSql  |        2         |
| RocketMQ | 4.9.2 |  消息中间件  |        2         |
| Nacos | 1.4.2 |  配置中心/注册中心  |        2         |
| ElasticSearch | 7.15.2 | 搜素引擎   |        2         |
| xxx-job | 2.3.0 | 定时任务   |        2         |
| Seata | 1.4.2 |  分布式事务  |        3         |
| Elastic Stack | 7.15.2 |  监控日志  |        3         |
| Skywalking | 9.0.0 |  分布式链路追踪  |        3         |
| Grafana | 8.5.0 |  系统监控展示页面 |        3         |
| Prometheus | 2.35.0 |  系统监控平台  |        3         |

## 参考文档/项目
<https://martinfowler.com/bliki/DDD_Aggregate.html>

<https://mp.weixin.qq.com/s/1bcymUcjCkOdvVygunShmw>

<https://github.com/citerus/dddsample-core>

### 项目
### 商品&购物车服务
https://gitee.com/p7-architect/p7-store.git
- 启动
1. 执行sql脚本
2. 配置mysql

### 用户服务&前端项目
https://gitee.com/p7-architect/gupao-p7store-parent.git、
- 后端启动
1. 执行sql脚本
2. 配置mysql
3. 配置redis
- 前端启动
1. 安装node
2. npm install
3. npm run dev



