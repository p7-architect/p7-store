package com.p7.architect.store.product.domain.model.product;


import com.p7.architect.store.product.domain.model.store.Store;
import com.p7.architect.store.product.domain.shared.Entity;
import lombok.Data;

import java.util.List;

/**
 * desc:
 *
 * @ClassName: com.gupaoedu.p7mall.domain.model.product.SpuProduct
 * @author: dengbicheng
 * @date: 2022-06-09 14:15
 * @version: v 1.0.0
 */
@Data
public class Spu implements Entity<Spu> {
    private SpuId spuId;
    private String spuName;
    private String spuDescription;
    private List<Sku> skuList;
    private Store store;

    public Spu(SpuId spuId, String spuName, List<Sku> skuList, Store store) {
        this.spuId = spuId;
        this.spuName = spuName;
        this.skuList = skuList;
        this.store = store;
    }

    public SpuId spuId(){
        return spuId;
    }

    public List<Sku> getSkuList() {
        return skuList;
    }

    public void setSkuList(List<Sku> skuList) {
        this.skuList = skuList;
    }

    @Override
    public boolean sameIdentityAs(final Spu other) {
        return other!=null && this.spuId.sameValueAs(other.spuId);
    }
}
