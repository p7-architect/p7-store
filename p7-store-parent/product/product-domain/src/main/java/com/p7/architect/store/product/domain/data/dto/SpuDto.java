package com.p7.architect.store.product.domain.data.dto;

import com.p7.architect.store.common.base.BaseEntity;
import com.p7.architect.store.common.validate.AddGroup;
import com.p7.architect.store.common.validate.EditGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Date;

import java.util.Date;


/**
 * 商品信息数据传输对象 pms_spu
 *
 * @ClassName: SpuDto
 * @author dengbicheng
 * @date 2022-07-06
 */

@Data
@ApiModel("商品信息业务对象")
public class SpuDto extends BaseEntity implements Serializable {

    /**
     * 商品id
     */
    @ApiModelProperty(value = "商品id", required = true)
    @NotBlank(message = "商品id不能为空", groups = { EditGroup.class })
    private Long spuId;

    /**
     * 产品名称
     */
    @ApiModelProperty(value = "产品名称", required = true)
    @NotBlank(message = "产品名称不能为空", groups = { AddGroup.class, EditGroup.class })
    private String spuName;

    /**
     * 商品描述
     */
    @ApiModelProperty(value = "商品描述", required = true)
    @NotBlank(message = "商品描述不能为空", groups = { AddGroup.class, EditGroup.class })
    private String spuDesc;

    /**
     * 商品重量
     */
    @ApiModelProperty(value = "商品重量", required = true)
    @NotBlank(message = "商品重量不能为空", groups = { AddGroup.class, EditGroup.class })
    private String weight;

    /**
     * 单位
     */
    @ApiModelProperty(value = "单位", required = true)
    @NotBlank(message = "单位不能为空", groups = { AddGroup.class, EditGroup.class })
    private String unit;

    /**
     * 上架状态;【0 - 下架，1 - 上架】
     */
    @ApiModelProperty(value = "上架状态;【0 - 下架，1 - 上架】", required = true)
    @NotBlank(message = "上架状态;【0 - 下架，1 - 上架】不能为空", groups = { AddGroup.class, EditGroup.class })
    private String publishStatus;

    /**
     * 分页大小
     */
    @ApiModelProperty("分页大小")
    private Integer pageSize;

    /**
     * 当前页数
     */
    @ApiModelProperty("当前页数")
    private Integer pageNum;

    /**
     * 排序列
     */
    @ApiModelProperty("排序列")
    private String orderByColumn;

    /**
     * 排序的方向desc或者asc
     */
    @ApiModelProperty(value = "排序的方向", example = "asc,desc")
    private String isAsc;

}
