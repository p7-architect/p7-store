package com.p7.architect.store.product.domain.model.product;

import com.p7.architect.store.product.domain.shared.Entity;
import lombok.Data;

import java.util.List;

/**
 * desc:
 *
 * @ClassName: com.gupaoedu.p7mall.domain.model.product.Sku
 * @author: dengbicheng
 * @date: 2022-06-09 14:29
 * @version: v 1.0.0
 */
@Data
public class Sku implements Entity<Sku> {
    private SkuId skuId;
    private Long spuId;
    private String skuTitle;
    private Integer price;
    private Integer stockNum;
    private String skuDefaultImg;

    public Sku() {
    }

    public Sku(SkuId skuId, Long spuId, String skuTitle, Integer price, Integer stockNum, String skuDefaultImg) {
        this.skuId = skuId;
        this.spuId = spuId;
        this.skuTitle = skuTitle;
        this.price = price;
        this.stockNum = stockNum;
        this.skuDefaultImg = skuDefaultImg;
    }

    @Override
    public boolean sameIdentityAs(Sku other) {
        return other != null && skuId.equals(other.skuId);
    }
}
