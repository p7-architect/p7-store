package com.p7.architect.store.product.domain.model.store;


import com.p7.architect.store.product.domain.shared.ValueObject;
import lombok.Data;


@Data
public class StoreId implements ValueObject<StoreId> {

    private Long id;

    @Override
    public boolean sameValueAs(StoreId other) {
        return other != null && this.id.equals(other.id);
    }
}
