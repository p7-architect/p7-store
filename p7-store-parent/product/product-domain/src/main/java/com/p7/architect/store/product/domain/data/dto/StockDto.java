package com.p7.architect.store.product.domain.data.dto;

import com.p7.architect.store.common.base.BaseEntity;
import com.p7.architect.store.common.validate.AddGroup;
import com.p7.architect.store.common.validate.EditGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import javax.validation.constraints.*;

import java.io.Serializable;
import java.util.Date;

import java.util.Date;


/**
 * 商品库存数据传输对象 pms_stock
 *
 * @ClassName: StockDto
 * @author dengbicheng
 * @date 2022-07-06
 */

@Data
@ApiModel("商品库存业务对象")
public class StockDto extends BaseEntity implements Serializable {

    /**
     * id
     */
    @ApiModelProperty(value = "id", required = true)
    @NotBlank(message = "id不能为空", groups = { EditGroup.class })
    private Long id;

    /**
     * skuId
     */
    @ApiModelProperty(value = "skuId", required = true)
    @NotBlank(message = "skuId不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long skuId;

    /**
     * 库存数量
     */
    @ApiModelProperty(value = "库存数量", required = true)
    @NotNull(message = "库存数量不能为空", groups = { AddGroup.class, EditGroup.class })
    private Integer stockCount;

    /**
     * 分页大小
     */
    @ApiModelProperty("分页大小")
    private Integer pageSize;

    /**
     * 当前页数
     */
    @ApiModelProperty("当前页数")
    private Integer pageNum;

    /**
     * 排序列
     */
    @ApiModelProperty("排序列")
    private String orderByColumn;

    /**
     * 排序的方向desc或者asc
     */
    @ApiModelProperty(value = "排序的方向", example = "asc,desc")
    private String isAsc;

}
