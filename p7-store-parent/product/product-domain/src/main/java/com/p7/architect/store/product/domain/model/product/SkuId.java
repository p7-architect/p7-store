package com.p7.architect.store.product.domain.model.product;


import com.p7.architect.store.product.domain.shared.ValueObject;

/**
 * desc:
 *
 * @ClassName: com.gupaoedu.p7mall.domain.model.product.ProductId
 * @author: dengbicheng
 * @date: 2022-06-09 14:34
 * @version: v 1.0.0
 */
public class SkuId implements ValueObject<SkuId> {

    private Long id;

    @Override
    public boolean sameValueAs(SkuId other) {
        return other != null && this.id.equals(other.id);
    }
}
