package com.p7.architect.store.product.domain.data.dto;

import com.p7.architect.store.common.base.BaseEntity;
import com.p7.architect.store.common.validate.AddGroup;
import com.p7.architect.store.common.validate.EditGroup;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;


/**
 * 库存量单位数据传输对象 pms_sku
 *
 * @ClassName: SkuDto
 * @author dengbicheng
 * @date 2022-07-06
 */

@Data
@ApiModel("库存量单位业务对象")
public class SkuDto extends BaseEntity implements Serializable {

    /**
     * skuId
     */
    @ApiModelProperty(value = "skuId", required = true)
    @NotBlank(message = "skuId不能为空", groups = { EditGroup.class })
    private Long skuId;

    /**
     * spuId
     */
    @ApiModelProperty(value = "spuId", required = true)
    @NotBlank(message = "spuId不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long spuId;

    /**
     * 门店id
     */
    @ApiModelProperty(value = "门店id", required = true)
    @NotBlank(message = "门店id不能为空", groups = { AddGroup.class, EditGroup.class })
    private Long storeId;

    /**
     * sku名称
     */
    @ApiModelProperty(value = "sku名称", required = true)
    @NotBlank(message = "sku名称不能为空", groups = { AddGroup.class, EditGroup.class })
    private String skuName;

    /**
     * 默认图片
     */
    @ApiModelProperty(value = "默认图片", required = true)
    @NotBlank(message = "默认图片不能为空", groups = { AddGroup.class, EditGroup.class })
    private String skuDefaultImg;

    /**
     * 标题
     */
    @ApiModelProperty(value = "标题", required = true)
    @NotBlank(message = "标题不能为空", groups = { AddGroup.class, EditGroup.class })
    private String skuTitle;

    /**
     * 副标题
     */
    @ApiModelProperty(value = "副标题", required = true)
    @NotBlank(message = "副标题不能为空", groups = { AddGroup.class, EditGroup.class })
    private String skuSubtitle;

    /**
     * 价格
     */
    @ApiModelProperty(value = "价格", required = true)
    @NotNull(message = "价格不能为空", groups = { AddGroup.class, EditGroup.class })
    private Integer price;

    /**
     * 币种
     */
    @ApiModelProperty(value = "币种", required = true)
    @NotBlank(message = "币种不能为空", groups = { AddGroup.class, EditGroup.class })
    private String currency;

    /**
     * 库存
     */
    @ApiModelProperty(value = "库存", required = true)
    @NotNull(message = "库存不能为空", groups = { AddGroup.class, EditGroup.class })
    private Integer stockNum;

    /**
     * 销量
     */
    @ApiModelProperty(value = "销量", required = true)
    @NotNull(message = "销量不能为空", groups = { AddGroup.class, EditGroup.class })
    private Integer saleCount;

    /**
     * sku介绍描述
     */
    @ApiModelProperty(value = "sku介绍描述", required = true)
    @NotBlank(message = "sku介绍描述不能为空", groups = { AddGroup.class, EditGroup.class })
    private String skuDesc;

    /**
     * 分页大小
     */
    @ApiModelProperty("分页大小")
    private Integer pageSize;

    /**
     * 当前页数
     */
    @ApiModelProperty("当前页数")
    private Integer pageNum;

    /**
     * 排序列
     */
    @ApiModelProperty("排序列")
    private String orderByColumn;

    /**
     * 排序的方向desc或者asc
     */
    @ApiModelProperty(value = "排序的方向", example = "asc,desc")
    private String isAsc;

}
