package com.p7.architect.store.product.domain.model.store;

import com.p7.architect.store.product.domain.shared.Entity;

/**
 * desc:
 *
 * @ClassName: com.gupaoedu.p7mall.domain.model.store.Store
 * @author: dengbicheng
 * @date: 2022-06-09 14:24
 * @version: v 1.0.0
 */
public class Store implements Entity<Store> {
    private StoreId storeId;
    private String storeName;

    @Override
    public boolean sameIdentityAs(Store other) {
        return false;
    }
}
