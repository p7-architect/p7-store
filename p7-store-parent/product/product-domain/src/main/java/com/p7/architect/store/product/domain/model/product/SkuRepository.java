package com.p7.architect.store.product.domain.model.product;

import com.p7.architect.store.common.page.TableDataInfo;
import com.p7.architect.store.product.domain.data.vo.SkuVo;
import com.p7.architect.store.product.domain.model.store.StoreId;

import java.util.List;

public interface SkuRepository {

    List<SkuVo> getSkusBySpuId(SpuId spuId);

    List<Sku> getProductList(StoreId storeId);


    List<SkuVo> getSkuListBySkuIds(List<Long> skuIds);

    TableDataInfo<SkuVo> searchSkuList(StoreId storeId, String search, Integer pageSize, Integer pageNum);
}
