package com.p7.architect.store.product.domain.model.product;


import com.p7.architect.store.product.domain.shared.ValueObject;
import lombok.Data;

/**
 * desc:
 *
 * @ClassName: com.gupaoedu.p7mall.domain.model.product.ProductId
 * @author: dengbicheng
 * @date: 2022-06-09 14:34
 * @version: v 1.0.0
 */
@Data
public class SpuId implements ValueObject<SpuId> {

    private Long id;

    @Override
    public boolean sameValueAs(SpuId other) {
        return other != null && this.id.equals(other.id);
    }
}
