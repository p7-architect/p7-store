package com.p7.architect.store.product.domain.data.vo;

import java.util.Date;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;
import java.util.Date;

import com.p7.architect.store.common.base.BaseEntity;



/**
 * 商品信息视图对象 pms_spu
 *
 * @ClassName: SpuVo
 * @author dengbicheng
 * @date 2022-07-06
 */
@Data
@ApiModel("商品信息视图对象")
public class SpuVo extends BaseEntity implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * 商品id
     */

	@ApiModelProperty("商品id")
	private Long spuId;

    /**
     * 产品名称
     */

	@ApiModelProperty("产品名称")
	private String spuName;

    /**
     * 商品描述
     */

	@ApiModelProperty("商品描述")
	private String spuDesc;

    /**
     * 商品重量
     */

	@ApiModelProperty("商品重量")
	private String weight;

    /**
     * 单位
     */

	@ApiModelProperty("单位")
	private String unit;

    /**
     * 上架状态;【0 - 下架，1 - 上架】
     */

	@ApiModelProperty("上架状态;【0 - 下架，1 - 上架】")
	private String publishStatus;

}
