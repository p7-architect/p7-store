package com.p7.architect.store.product.domain.model.product;

import java.util.List;

public interface SpuRepository {
    Spu getSpu(String spuId);

    List<Spu> getSpuList(Long storeId,Integer page);
}
