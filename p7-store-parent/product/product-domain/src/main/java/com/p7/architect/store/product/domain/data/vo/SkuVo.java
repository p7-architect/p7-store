package com.p7.architect.store.product.domain.data.vo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;



/**
 * 库存量单位视图对象 pms_sku
 *
 * @ClassName: SkuVo
 * @author dengbicheng
 * @date 2022-07-06
 */
@Data
@ApiModel("库存量单位视图对象")
public class SkuVo implements Serializable {

	private static final long serialVersionUID = 1L;

    private Long skuId;

    private Long spuId;

    /**
     * 门店id
     */

	@ApiModelProperty("门店id")
	private Long storeId;

    /**
     * sku名称
     */

	@ApiModelProperty("sku名称")
	private String skuName;

    /**
     * 默认图片
     */

	@ApiModelProperty("默认图片")
	private String skuDefaultImg;

    /**
     * 标题
     */

	@ApiModelProperty("标题")
	private String skuTitle;

    /**
     * 副标题
     */

	@ApiModelProperty("副标题")
	private String skuSubtitle;

    /**
     * 价格
     */

	@ApiModelProperty("价格")
	private Integer price;

    /**
     * 币种
     */

	@ApiModelProperty("币种")
	private String currency;

	/**
	 * 库存
	 */

	@ApiModelProperty("库存")
	private Integer stockNum;

	/**
	 * 操作数量
	 */

	@ApiModelProperty("操作数量")
	private Integer num = 0;

    /**
     * 销量
     */

	@ApiModelProperty("销量")
	private Integer saleCount;

    /**
     * sku介绍描述
     */

	@ApiModelProperty("sku介绍描述")
	private String skuDesc;
}
