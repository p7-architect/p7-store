package com.p7.architect.store.product.domain.service;


import com.p7.architect.store.product.domain.model.product.Spu;

public interface SpuDomainService {
    Spu getSpu(Long spuId);
}
