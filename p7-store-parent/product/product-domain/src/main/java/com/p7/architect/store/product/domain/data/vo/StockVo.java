package com.p7.architect.store.product.domain.data.vo;

import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.io.Serializable;
import java.util.Date;

import com.p7.architect.store.common.base.BaseEntity;



/**
 * 商品库存视图对象 pms_stock
 *
 * @ClassName: StockVo
 * @author dengbicheng
 * @date 2022-07-06
 */
@Data
@ApiModel("商品库存视图对象")
public class StockVo extends BaseEntity implements Serializable {

	private static final long serialVersionUID = 1L;

    /**
     * id
     */

	@ApiModelProperty("id")
	private Long id;

    /**
     * skuId
     */

	@ApiModelProperty("skuId")
	private Long skuId;

    /**
     * 库存数量
     */

	@ApiModelProperty("库存数量")
	private Integer stockCount;


}
