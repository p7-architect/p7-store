package com.p7.architect.store.product.infrastructure.repositoryimpl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.p7.architect.store.product.domain.model.product.Spu;
import com.p7.architect.store.product.domain.model.product.SpuRepository;
import com.p7.architect.store.product.infrastructure.convert.SpuConvert;
import com.p7.architect.store.product.infrastructure.domain.SpuDo;
import com.p7.architect.store.product.infrastructure.service.ISkuService;
import com.p7.architect.store.product.infrastructure.service.ISpuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class SpuRepositoryImpl implements SpuRepository {

    @Autowired
    private ISpuService spuServiceImpl;

    @Override
    public Spu getSpu(String spuId) {
        return null;
    }

    @Override
    public List<Spu> getSpuList(Long storeId, Integer page) {
        LambdaQueryWrapper<SpuDo> lqw = Wrappers.lambdaQuery(SpuDo.class);

        lqw.eq(SpuDo::getStoreId,storeId);
        List<SpuDo> list = spuServiceImpl.list(lqw);
        if(list==null) {
            return null;
        }

        return SpuConvert.IMPL.bList2aList(list);
    }
}
