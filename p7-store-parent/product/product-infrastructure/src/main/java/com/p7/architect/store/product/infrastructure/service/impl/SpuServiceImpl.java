package com.p7.architect.store.product.infrastructure.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.p7.architect.store.common.page.PageUtils;
import com.p7.architect.store.common.page.PagePlus;
import com.p7.architect.store.common.page.TableDataInfo;
import com.p7.architect.store.product.domain.data.dto.SpuDto;
import com.p7.architect.store.product.domain.data.vo.SpuVo;
import org.springframework.stereotype.Service;
import com.p7.architect.store.common.mybatisplus.core.ServicePlusImpl;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;

import com.p7.architect.store.product.infrastructure.domain.SpuDo;
import com.p7.architect.store.product.infrastructure.mapper.SpuMapper;
import com.p7.architect.store.product.infrastructure.service.ISpuService;

import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 商品信息Service业务层处理
 *
 * @ClassName: ISpuServiceImpl
 * @author dengbicheng
 * @date 2022-07-06
 */
@Service
public class SpuServiceImpl extends ServicePlusImpl<SpuMapper, SpuDo, SpuVo> implements ISpuService {

    @Override
    public SpuVo queryById(Long spuId){
        return getVoById(spuId);
    }

    @Override
    public TableDataInfo<SpuVo> queryPageList(SpuDto dto) {
        PagePlus<SpuDo, SpuVo> result = pageVo(PageUtils.buildPagePlus(), buildQueryWrapper(dto));
        return PageUtils.buildDataInfo(result);
    }

    @Override
    public List<SpuVo> queryList(SpuDto dto) {
        return listVo(buildQueryWrapper(dto));
    }

    private LambdaQueryWrapper<SpuDo> buildQueryWrapper(SpuDto dto) {
        Map<String, Object> params = dto.getParams();
        LambdaQueryWrapper<SpuDo> lqw = Wrappers.lambdaQuery();
        lqw.like(StringUtils.isNotBlank(dto.getSpuName()), SpuDo::getSpuName, dto.getSpuName());
        lqw.eq(StringUtils.isNotBlank(dto.getSpuDesc()), SpuDo::getSpuDesc, dto.getSpuDesc());
        lqw.eq(StringUtils.isNotBlank(dto.getWeight()), SpuDo::getWeight, dto.getWeight());
        lqw.eq(StringUtils.isNotBlank(dto.getUnit()), SpuDo::getUnit, dto.getUnit());
        lqw.eq(StringUtils.isNotBlank(dto.getPublishStatus()), SpuDo::getPublishStatus, dto.getPublishStatus());
        return lqw;
    }

    @Override
    public Boolean insertByDto(SpuDto dto) {
        SpuDo add = BeanUtil.toBean(dto, SpuDo.class);
        validEntityBeforeSave(add);
        boolean flag = save(add);
        if (flag) {
            dto.setSpuId(add.getSpuId());
        }
        return flag;
    }

    @Override
    public Boolean updateByDto(SpuDto dto) {
        SpuDo update = BeanUtil.toBean(dto, SpuDo.class);
        validEntityBeforeSave(update);
        return updateById(update);
    }

    /**
     * 保存前的数据校验
     *
     * @param entity 实体类数据
     */
    private void validEntityBeforeSave(SpuDo entity){
        //TODO 做一些数据校验,如唯一约束
    }

    @Override
    public Boolean deleteWithValidByIds(Collection<String> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return removeByIds(ids);
    }
}
