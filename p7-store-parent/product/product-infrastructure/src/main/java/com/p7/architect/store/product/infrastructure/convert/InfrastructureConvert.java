package com.p7.architect.store.product.infrastructure.convert;

import java.util.List;


public interface InfrastructureConvert<A, B> {
    B a2b(A a);

    List<B> alist2bList(List<A> aList);

    A b2a(B b);

    List<A> bList2aList(List<B> bList);


}
