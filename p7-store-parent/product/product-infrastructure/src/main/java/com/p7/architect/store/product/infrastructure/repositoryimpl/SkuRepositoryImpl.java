package com.p7.architect.store.product.infrastructure.repositoryimpl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.p7.architect.store.common.page.PageUtils;
import com.p7.architect.store.common.page.TableDataInfo;
import com.p7.architect.store.product.domain.data.vo.SkuVo;
import com.p7.architect.store.product.domain.model.product.Sku;
import com.p7.architect.store.product.domain.model.product.SkuRepository;
import com.p7.architect.store.product.domain.model.product.SpuId;
import com.p7.architect.store.product.domain.model.store.StoreId;
import com.p7.architect.store.product.infrastructure.convert.SkuConvert;
import com.p7.architect.store.product.infrastructure.domain.SkuDo;
import com.p7.architect.store.product.infrastructure.service.ISkuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class SkuRepositoryImpl implements SkuRepository {

    @Autowired
    private ISkuService skuServiceImpl;


    @Override
    public List<SkuVo> getSkusBySpuId(SpuId spuId) {
        LambdaQueryWrapper<SkuDo> lqw = Wrappers.lambdaQuery(SkuDo.class);
        lqw.eq(spuId!=null,SkuDo::getSpuId,spuId.getId());
        List<SkuDo> list = skuServiceImpl.list(lqw);
        return SkuConvert.IMPL.bList2aList(list);
    }

    @Override
    public List<Sku> getProductList(StoreId storeId) {
        return null;
    }

    @Override
    public List<SkuVo> getSkuListBySkuIds(List<Long> skuIds) {
        LambdaQueryWrapper<SkuDo> lqw = Wrappers.lambdaQuery(SkuDo.class);
        lqw.in(SkuDo::getSkuId,skuIds);
        return skuServiceImpl.listVo(lqw);
    }

    @Override
    public TableDataInfo<SkuVo> searchSkuList(StoreId storeId,String search,Integer pageSize,Integer pageNum) {
        LambdaQueryWrapper<SkuDo> lqw = Wrappers.lambdaQuery(SkuDo.class);
        lqw.eq(SkuDo::getStoreId,storeId.getId());
        lqw.likeRight(StringUtils.isNotBlank(search),SkuDo::getSkuName,search);

        return PageUtils.buildDataInfo(skuServiceImpl.pageVo(PageUtils.buildPagePlus(), lqw));
    }

}
