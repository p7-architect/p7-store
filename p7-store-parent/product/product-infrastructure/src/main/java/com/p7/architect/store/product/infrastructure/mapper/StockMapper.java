package com.p7.architect.store.product.infrastructure.mapper;

import com.p7.architect.store.product.infrastructure.domain.StockDo;
import com.p7.architect.store.common.mybatisplus.core.BaseMapperPlus;

/**
 * 商品库存Mapper接口
 *
 * @ClassName: StockMapper
 * @author dengbicheng
 * @date 2022-07-06
 */
public interface StockMapper extends BaseMapperPlus<StockDo> {

}
