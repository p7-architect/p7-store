package com.p7.architect.store.product.infrastructure.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;
import com.p7.architect.store.common.base.BaseEntity;

import java.util.Date;


/**
 * 商品信息对象 pms_spu
 *
 * @ClassName: SpuDo
 * @author dengbicheng
 * @date 2022-07-06
 */
@Data
@Accessors(chain = true)
@TableName("pms_spu")
public class SpuDo extends BaseEntity implements Serializable {

    private static final long serialVersionUID=1L;

    /**
     * 商品id
     */
    @TableId(value = "spu_id")
    private Long spuId;
    /**
     * 门店id
     */
    @TableField(value = "store_id")
    private Long storeId;
    /**
     * 产品名称
     */
    @TableField(value = "spu_name")
    private String spuName;
    /**
     * 商品描述
     */
    @TableField(value = "spu_desc")
    private String spuDesc;
    /**
     * 商品重量
     */
    @TableField(value = "weight")
    private String weight;
    /**
     * 单位
     */
    @TableField(value = "unit")
    private String unit;
    /**
     * 上架状态;【0 - 下架，1 - 上架】
     */
    @TableField(value = "publish_status")
    private String publishStatus;

}
