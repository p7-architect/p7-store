package com.p7.architect.store.product.infrastructure.service;

import com.p7.architect.store.product.domain.data.dto.SkuDto;
import com.p7.architect.store.product.domain.data.vo.SkuVo;
import com.p7.architect.store.product.infrastructure.domain.SkuDo;
import com.p7.architect.store.common.mybatisplus.core.IServicePlus;
import com.p7.architect.store.common.page.TableDataInfo;

import java.util.Collection;
import java.util.List;

/**
 * 库存量单位Service接口
 *
 * @ClassName: ISkuService
 * @author dengbicheng
 * @date 2022-07-06
 */
public interface ISkuService extends IServicePlus<SkuDo, SkuVo> {
	/**
	 * 查询单个
     * @param skuId 主键ID
	 * @return  SkuVo  库存量单位Vo对象
	 */
	SkuVo queryById(Long skuId);

	/**
	 * 查询带分页列表
	 * @param dto SkuDto 条件对象
     * @return  TableDataInfo<SkuVo> 分页对象
	 */
    TableDataInfo<SkuVo> queryPageList(SkuDto dto);

	/**
	 * 查询列表
	 * @param dto SkuDto 条件对象
     * @return  List<SkuVo>  库存量单位集合
     */
	List<SkuVo> queryList(SkuDto dto);

	/**
	 * 根据新增业务对象插入库存量单位
	 * @param dto 库存量单位新增业务对象
	 * @return Boolean
	 */
	Boolean insertByDto(SkuDto dto);

	/**
	 * 根据编辑业务对象修改库存量单位
	 * @param dto 库存量单位编辑业务对象
	 * @return Boolean
	 */
	Boolean updateByDto(SkuDto dto);

	/**
	 * 校验并删除数据
	 * @param ids 主键集合
	 * @param isValid 是否校验,true-删除前校验,false-不校验
	 * @return Boolean
	 */
	Boolean deleteWithValidByIds(Collection<String> ids, Boolean isValid);
}
