package com.p7.architect.store.product.infrastructure.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;
import com.p7.architect.store.common.base.BaseEntity;

import java.util.Date;


/**
 * 商品库存对象 pms_stock
 *
 * @ClassName: StockDo
 * @author dengbicheng
 * @date 2022-07-06
 */
@Data
@Accessors(chain = true)
@TableName("pms_stock")
public class StockDo extends BaseEntity implements Serializable {

    private static final long serialVersionUID=1L;

    /**
     * id
     */
    @TableId(value = "id")
    private Long id;
    /**
     * skuId
     */
    @TableField(value = "sku_id")
    private Long skuId;
    /**
     * 库存数量
     */
    @TableField(value = "stock_count")
    private Integer stockCount;

}
