package com.p7.architect.store.product.infrastructure.convert;

import com.p7.architect.store.product.domain.model.product.Sku;
import com.p7.architect.store.product.domain.model.product.Spu;
import com.p7.architect.store.product.infrastructure.domain.SkuDo;
import com.p7.architect.store.product.infrastructure.domain.SpuDo;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * Sku转换
 *
 * @author dbc
 * @date 2022/06/12
 */
@Mapper
public interface SpuConvert extends InfrastructureConvert<Spu, SpuDo> {
    SpuConvert IMPL = Mappers.getMapper(SpuConvert.class);
}
