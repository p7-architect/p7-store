package com.p7.architect.store.product.infrastructure.mapper;

import com.p7.architect.store.product.infrastructure.domain.SpuDo;
import com.p7.architect.store.common.mybatisplus.core.BaseMapperPlus;

/**
 * 商品信息Mapper接口
 *
 * @ClassName: SpuMapper
 * @author dengbicheng
 * @date 2022-07-06
 */
public interface SpuMapper extends BaseMapperPlus<SpuDo> {

}
