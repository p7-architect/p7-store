package com.p7.architect.store.product.infrastructure.service;

import com.p7.architect.store.common.mybatisplus.core.IServicePlus;
import com.p7.architect.store.common.page.TableDataInfo;
import com.p7.architect.store.product.domain.data.dto.StockDto;
import com.p7.architect.store.product.domain.data.vo.StockVo;
import com.p7.architect.store.product.infrastructure.domain.StockDo;


import java.util.Collection;
import java.util.List;

/**
 * 商品库存Service接口
 *
 * @ClassName: IStockService
 * @author dengbicheng
 * @date 2022-07-06
 */
public interface IStockService extends IServicePlus<StockDo, StockVo> {
	/**
	 * 查询单个
     * @param id 主键ID
	 * @return  StockVo  商品库存Vo对象
	 */
	StockVo queryById(Long id);

	/**
	 * 查询带分页列表
	 * @param dto StockDto 条件对象
     * @return  TableDataInfo<StockVo> 分页对象
	 */
    TableDataInfo<StockVo> queryPageList(StockDto dto);

	/**
	 * 查询列表
	 * @param dto StockDto 条件对象
     * @return  List<StockVo>  商品库存集合
     */
	List<StockVo> queryList(StockDto dto);

	/**
	 * 根据新增业务对象插入商品库存
	 * @param dto 商品库存新增业务对象
	 * @return Boolean
	 */
	Boolean insertByDto(StockDto dto);

	/**
	 * 根据编辑业务对象修改商品库存
	 * @param dto 商品库存编辑业务对象
	 * @return Boolean
	 */
	Boolean updateByDto(StockDto dto);

	/**
	 * 校验并删除数据
	 * @param ids 主键集合
	 * @param isValid 是否校验,true-删除前校验,false-不校验
	 * @return Boolean
	 */
	Boolean deleteWithValidByIds(Collection<String> ids, Boolean isValid);
}
