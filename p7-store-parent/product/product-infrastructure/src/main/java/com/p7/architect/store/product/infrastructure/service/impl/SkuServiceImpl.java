package com.p7.architect.store.product.infrastructure.service.impl;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.p7.architect.store.common.page.PageUtils;
import com.p7.architect.store.common.page.PagePlus;
import com.p7.architect.store.common.page.TableDataInfo;
import com.p7.architect.store.product.domain.data.dto.SkuDto;
import com.p7.architect.store.product.domain.data.vo.SkuVo;
import org.springframework.stereotype.Service;
import com.p7.architect.store.common.mybatisplus.core.ServicePlusImpl;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;

import com.p7.architect.store.product.infrastructure.domain.SkuDo;
import com.p7.architect.store.product.infrastructure.mapper.SkuMapper;
import com.p7.architect.store.product.infrastructure.service.ISkuService;

import java.util.List;
import java.util.Map;
import java.util.Collection;

/**
 * 库存量单位Service业务层处理
 *
 * @ClassName: ISkuServiceImpl
 * @author dengbicheng
 * @date 2022-07-06
 */
@Service
public class SkuServiceImpl extends ServicePlusImpl<SkuMapper, SkuDo, SkuVo> implements ISkuService {

    @Override
    public SkuVo queryById(Long skuId){
        return getVoById(skuId);
    }

    @Override
    public TableDataInfo<SkuVo> queryPageList(SkuDto dto) {
        PagePlus<SkuDo, SkuVo> result = pageVo(PageUtils.buildPagePlus(), buildQueryWrapper(dto));
        return PageUtils.buildDataInfo(result);
    }

    @Override
    public List<SkuVo> queryList(SkuDto dto) {
        return listVo(buildQueryWrapper(dto));
    }

    private LambdaQueryWrapper<SkuDo> buildQueryWrapper(SkuDto dto) {
        Map<String, Object> params = dto.getParams();
        LambdaQueryWrapper<SkuDo> lqw = Wrappers.lambdaQuery();
        lqw.eq(dto.getSpuId()!=null, SkuDo::getSpuId, dto.getSpuId());
        lqw.eq(dto.getStoreId()!=null, SkuDo::getStoreId, dto.getStoreId());
        lqw.like(StringUtils.isNotBlank(dto.getSkuName()), SkuDo::getSkuName, dto.getSkuName());
        lqw.eq(StringUtils.isNotBlank(dto.getSkuDefaultImg()), SkuDo::getSkuDefaultImg, dto.getSkuDefaultImg());
        lqw.eq(StringUtils.isNotBlank(dto.getSkuTitle()), SkuDo::getSkuTitle, dto.getSkuTitle());
        lqw.eq(StringUtils.isNotBlank(dto.getSkuSubtitle()), SkuDo::getSkuSubtitle, dto.getSkuSubtitle());
        lqw.eq(dto.getPrice() != null, SkuDo::getPrice, dto.getPrice());
        lqw.eq(StringUtils.isNotBlank(dto.getCurrency()), SkuDo::getCurrency, dto.getCurrency());
        lqw.eq(dto.getSaleCount() != null, SkuDo::getSaleCount, dto.getSaleCount());
        lqw.eq(StringUtils.isNotBlank(dto.getSkuDesc()), SkuDo::getSkuDesc, dto.getSkuDesc());
        return lqw;
    }

    @Override
    public Boolean insertByDto(SkuDto dto) {
        SkuDo add = BeanUtil.toBean(dto, SkuDo.class);
        validEntityBeforeSave(add);
        boolean flag = save(add);
        if (flag) {
            dto.setSkuId(add.getSkuId());
        }
        return flag;
    }

    @Override
    public Boolean updateByDto(SkuDto dto) {
        SkuDo update = BeanUtil.toBean(dto, SkuDo.class);
        validEntityBeforeSave(update);
        return updateById(update);
    }

    /**
     * 保存前的数据校验
     *
     * @param entity 实体类数据
     */
    private void validEntityBeforeSave(SkuDo entity){
        //TODO 做一些数据校验,如唯一约束
    }

    @Override
    public Boolean deleteWithValidByIds(Collection<String> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return removeByIds(ids);
    }
}
