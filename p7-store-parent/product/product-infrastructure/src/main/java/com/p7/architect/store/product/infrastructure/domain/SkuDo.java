package com.p7.architect.store.product.infrastructure.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;
import com.p7.architect.store.common.base.BaseEntity;

import java.util.Date;


/**
 * 库存量单位对象 pms_sku
 *
 * @ClassName: SkuDo
 * @author dengbicheng
 * @date 2022-07-06
 */
@Data
@Accessors(chain = true)
@TableName("pms_sku")
public class SkuDo extends BaseEntity implements Serializable {

    private static final long serialVersionUID=1L;

    /**
     * skuId
     */
    @TableId(value = "sku_id")
    private Long skuId;
    /**
     * spuId
     */
    @TableField(value = "spu_id")
    private Long spuId;
    /**
     * 门店id
     */
    @TableField(value = "store_id")
    private Long storeId;
    /**
     * sku名称
     */
    @TableField(value = "sku_name")
    private String skuName;
    /**
     * 默认图片
     */
    @TableField(value = "sku_default_img")
    private String skuDefaultImg;
    /**
     * 标题
     */
    @TableField(value = "sku_title")
    private String skuTitle;
    /**
     * 副标题
     */
    @TableField(value = "sku_subtitle")
    private String skuSubtitle;
    /**
     * 价格
     */
    @TableField(value = "price")
    private Integer price;
    /**
     * 币种
     */
    @TableField(value = "currency")
    private String currency;

    /**
     * 库存
     */
    @TableField(value = "stock_num")
    private Integer stockNum;
    /**
     * 销量
     */
    @TableField(value = "sale_count")
    private Integer saleCount;
    /**
     * sku介绍描述
     */
    @TableField(value = "sku_desc")
    private String skuDesc;


}
