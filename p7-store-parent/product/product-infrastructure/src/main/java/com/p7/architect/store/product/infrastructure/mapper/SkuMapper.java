package com.p7.architect.store.product.infrastructure.mapper;

import com.p7.architect.store.product.infrastructure.domain.SkuDo;
import com.p7.architect.store.common.mybatisplus.core.BaseMapperPlus;

/**
 * 库存量单位Mapper接口
 *
 * @ClassName: SkuMapper
 * @author dengbicheng
 * @date 2022-07-06
 */
public interface SkuMapper extends BaseMapperPlus<SkuDo> {

}
