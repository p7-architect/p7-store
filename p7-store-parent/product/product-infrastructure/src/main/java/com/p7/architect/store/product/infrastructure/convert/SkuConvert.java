package com.p7.architect.store.product.infrastructure.convert;

import com.p7.architect.store.product.domain.data.vo.SkuVo;
import com.p7.architect.store.product.domain.model.product.Sku;
import com.p7.architect.store.product.infrastructure.domain.SkuDo;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * Sku转换
 *
 * @author dbc
 * @date 2022/06/12
 */
@Mapper
public interface SkuConvert extends InfrastructureConvert<SkuVo, SkuDo> {
    SkuConvert IMPL = Mappers.getMapper(SkuConvert.class);
}
