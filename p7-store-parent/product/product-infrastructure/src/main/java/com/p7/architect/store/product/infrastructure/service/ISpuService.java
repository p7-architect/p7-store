package com.p7.architect.store.product.infrastructure.service;

import com.p7.architect.store.common.mybatisplus.core.IServicePlus;
import com.p7.architect.store.common.page.TableDataInfo;
import com.p7.architect.store.product.domain.data.dto.SpuDto;
import com.p7.architect.store.product.domain.data.vo.SpuVo;
import com.p7.architect.store.product.infrastructure.domain.SpuDo;


import java.util.Collection;
import java.util.List;

/**
 * 商品信息Service接口
 *
 * @ClassName: ISpuService
 * @author dengbicheng
 * @date 2022-07-06
 */
public interface ISpuService extends IServicePlus<SpuDo, SpuVo> {
	/**
	 * 查询单个
     * @param spuId 主键ID
	 * @return  SpuVo  商品信息Vo对象
	 */
	SpuVo queryById(Long spuId);

	/**
	 * 查询带分页列表
	 * @param dto SpuDto 条件对象
     * @return  TableDataInfo<SpuVo> 分页对象
	 */
    TableDataInfo<SpuVo> queryPageList(SpuDto dto);

	/**
	 * 查询列表
	 * @param dto SpuDto 条件对象
     * @return  List<SpuVo>  商品信息集合
     */
	List<SpuVo> queryList(SpuDto dto);

	/**
	 * 根据新增业务对象插入商品信息
	 * @param dto 商品信息新增业务对象
	 * @return Boolean
	 */
	Boolean insertByDto(SpuDto dto);

	/**
	 * 根据编辑业务对象修改商品信息
	 * @param dto 商品信息编辑业务对象
	 * @return Boolean
	 */
	Boolean updateByDto(SpuDto dto);

	/**
	 * 校验并删除数据
	 * @param ids 主键集合
	 * @param isValid 是否校验,true-删除前校验,false-不校验
	 * @return Boolean
	 */
	Boolean deleteWithValidByIds(Collection<String> ids, Boolean isValid);
}
