package com.p7.architect.store.product.api.web.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ProductResponse {

    private Long spuId;
    private String spuName;
    private String spuDescription;

    private List<SkuResponse> skuResponseList;
}
