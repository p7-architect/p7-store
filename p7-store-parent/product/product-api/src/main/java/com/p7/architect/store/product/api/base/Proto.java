package com.p7.architect.store.product.api.base;


public class Proto {

    private int code;
    private String msg;

    public static Proto SUCCESS = new Proto(0, "success");
    public static Proto SYSTEM_ERROR = new Proto(101, "网络开小差, 请稍后重试");

    public Proto(int code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

}
