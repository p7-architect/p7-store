package com.p7.architect.store.product.api.web.request;

import com.p7.architect.store.product.domain.model.store.StoreId;
import lombok.Data;

@Data
public class SkuRequest {

    StoreId storeId;
    String search;
    Integer pageSize;
    Integer pageNum;
}
