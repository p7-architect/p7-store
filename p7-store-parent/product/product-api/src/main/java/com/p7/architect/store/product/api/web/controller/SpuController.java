package com.p7.architect.store.product.api.web.controller;

import com.p7.architect.store.product.api.base.Result;
import com.p7.architect.store.product.api.web.request.ProductRequest;
import com.p7.architect.store.product.application.command.ListProductQuery;
import com.p7.architect.store.product.application.service.ProductService;


import com.p7.architect.store.product.domain.data.vo.SpuVo;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;


@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/spu")
public class SpuController {

    private final ProductService productService;


    @ResponseBody
    @PostMapping(value = "/list")
    public Result<List<SpuVo>> getSpuListInStore(@RequestBody ProductRequest request) {
        List<SpuVo> spuVoList = productService.executeListProductQuery(
                new ListProductQuery(request.getStoreId(), request.getPage())
        );
        return Result.success(spuVoList);
    }
}
