package com.p7.architect.store.product.api.web.request;

import lombok.Data;

@Data
public class ProductRequest {

    Long storeId;
    Integer page;
}
