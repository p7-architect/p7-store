package com.p7.architect.store.product.api.web.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SkuResponse {

    private Long skuId;
    private Long storeId;
    private String skuName;
    private String skuTitle;
    private String skuDefaultImg;
    private Integer price;
    private Integer stockNum;
    private Integer saleCount;
    private Integer totalAmount;
}
