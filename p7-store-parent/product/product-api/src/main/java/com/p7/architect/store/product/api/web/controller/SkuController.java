package com.p7.architect.store.product.api.web.controller;

import com.p7.architect.store.common.page.TableDataInfo;
import com.p7.architect.store.product.api.base.Result;
import com.p7.architect.store.product.api.web.request.SkuRequest;
import com.p7.architect.store.product.domain.data.vo.SkuVo;
import com.p7.architect.store.product.domain.model.product.SkuRepository;
import com.p7.architect.store.product.application.query.SkuInfoQuery;
import com.p7.architect.store.product.application.query.SkuInfoQueryService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping(value = "/product")
public class SkuController {

    private final SkuRepository skuRepository;

    private final SkuInfoQueryService skuInfoQueryService;

    @ResponseBody
    @PostMapping(value = "/listByIds")
    public Result<List<SkuVo>> getSkuListBySkuIds(@RequestBody List<Long> skuIds) {
        List<SkuVo> skuVoList = skuRepository.getSkuListBySkuIds(skuIds);
        return Result.success(skuVoList);
    }

    @ResponseBody
    @PostMapping(value = "/findSkuList")
    public TableDataInfo<SkuVo> findSkuList(@RequestBody SkuRequest skuRequest) {
        TableDataInfo<SkuVo> skuVoTableDataInfo = skuInfoQueryService.queryWithSearchAndStoreId(new SkuInfoQuery(
                skuRequest.getStoreId(),
                skuRequest.getSearch(),
                skuRequest.getPageSize(),
                skuRequest.getPageNum()
        ));
        return skuVoTableDataInfo;
    }
}
