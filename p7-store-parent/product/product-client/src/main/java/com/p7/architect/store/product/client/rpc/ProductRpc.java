package com.p7.architect.store.product.client.rpc;

import com.p7.architect.store.product.client.dto.ListProductRpcQuery;
import com.p7.architect.store.product.client.vo.SpuRpcVo;

import java.util.List;

/**
 * spu rpc
 *
 * @author dbc
 * @date 2022/06/12
 */
public interface ProductRpc {

    List<SpuRpcVo> getSpuList(ListProductRpcQuery listProductRpcQuery);
}
