package com.p7.architect.store.product.client.rpc;

import com.p7.architect.store.product.client.vo.SkuRpcVo;

import java.util.List;

/**
 * spu rpc
 *
 * @author dbc
 * @date 2022/06/12
 */
public interface SkuRpc {

    List<SkuRpcVo> getSkuListBySkuIdList(List<Long> skuIdList);
}
