package com.p7.architect.store.product.client.rpc.sku;

import static io.grpc.MethodDescriptor.generateFullMethodName;

/**
 */
@javax.annotation.Generated(
    value = "by gRPC proto compiler (version 1.45.1)",
    comments = "Source: sku.proto")
@io.grpc.stub.annotations.GrpcGenerated
public final class SkuServiceGrpc {

  private SkuServiceGrpc() {}

  public static final String SERVICE_NAME = "SkuService";

  // Static method descriptors that strictly reflect the proto.
  private static volatile io.grpc.MethodDescriptor<com.p7.architect.store.product.client.rpc.sku.SkuRequest,
      com.p7.architect.store.product.client.rpc.sku.SkuResponse> getGetSkuListBySkuIdListMethod;

  @io.grpc.stub.annotations.RpcMethod(
      fullMethodName = SERVICE_NAME + '/' + "getSkuListBySkuIdList",
      requestType = com.p7.architect.store.product.client.rpc.sku.SkuRequest.class,
      responseType = com.p7.architect.store.product.client.rpc.sku.SkuResponse.class,
      methodType = io.grpc.MethodDescriptor.MethodType.UNARY)
  public static io.grpc.MethodDescriptor<com.p7.architect.store.product.client.rpc.sku.SkuRequest,
      com.p7.architect.store.product.client.rpc.sku.SkuResponse> getGetSkuListBySkuIdListMethod() {
    io.grpc.MethodDescriptor<com.p7.architect.store.product.client.rpc.sku.SkuRequest, com.p7.architect.store.product.client.rpc.sku.SkuResponse> getGetSkuListBySkuIdListMethod;
    if ((getGetSkuListBySkuIdListMethod = SkuServiceGrpc.getGetSkuListBySkuIdListMethod) == null) {
      synchronized (SkuServiceGrpc.class) {
        if ((getGetSkuListBySkuIdListMethod = SkuServiceGrpc.getGetSkuListBySkuIdListMethod) == null) {
          SkuServiceGrpc.getGetSkuListBySkuIdListMethod = getGetSkuListBySkuIdListMethod =
              io.grpc.MethodDescriptor.<com.p7.architect.store.product.client.rpc.sku.SkuRequest, com.p7.architect.store.product.client.rpc.sku.SkuResponse>newBuilder()
              .setType(io.grpc.MethodDescriptor.MethodType.UNARY)
              .setFullMethodName(generateFullMethodName(SERVICE_NAME, "getSkuListBySkuIdList"))
              .setSampledToLocalTracing(true)
              .setRequestMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.p7.architect.store.product.client.rpc.sku.SkuRequest.getDefaultInstance()))
              .setResponseMarshaller(io.grpc.protobuf.ProtoUtils.marshaller(
                  com.p7.architect.store.product.client.rpc.sku.SkuResponse.getDefaultInstance()))
              .setSchemaDescriptor(new SkuServiceMethodDescriptorSupplier("getSkuListBySkuIdList"))
              .build();
        }
      }
    }
    return getGetSkuListBySkuIdListMethod;
  }

  /**
   * Creates a new async stub that supports all call types for the service
   */
  public static SkuServiceStub newStub(io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<SkuServiceStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<SkuServiceStub>() {
        @java.lang.Override
        public SkuServiceStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new SkuServiceStub(channel, callOptions);
        }
      };
    return SkuServiceStub.newStub(factory, channel);
  }

  /**
   * Creates a new blocking-style stub that supports unary and streaming output calls on the service
   */
  public static SkuServiceBlockingStub newBlockingStub(
      io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<SkuServiceBlockingStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<SkuServiceBlockingStub>() {
        @java.lang.Override
        public SkuServiceBlockingStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new SkuServiceBlockingStub(channel, callOptions);
        }
      };
    return SkuServiceBlockingStub.newStub(factory, channel);
  }

  /**
   * Creates a new ListenableFuture-style stub that supports unary calls on the service
   */
  public static SkuServiceFutureStub newFutureStub(
      io.grpc.Channel channel) {
    io.grpc.stub.AbstractStub.StubFactory<SkuServiceFutureStub> factory =
      new io.grpc.stub.AbstractStub.StubFactory<SkuServiceFutureStub>() {
        @java.lang.Override
        public SkuServiceFutureStub newStub(io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
          return new SkuServiceFutureStub(channel, callOptions);
        }
      };
    return SkuServiceFutureStub.newStub(factory, channel);
  }

  /**
   */
  public static abstract class SkuServiceImplBase implements io.grpc.BindableService {

    /**
     */
    public void getSkuListBySkuIdList(com.p7.architect.store.product.client.rpc.sku.SkuRequest request,
        io.grpc.stub.StreamObserver<com.p7.architect.store.product.client.rpc.sku.SkuResponse> responseObserver) {
      io.grpc.stub.ServerCalls.asyncUnimplementedUnaryCall(getGetSkuListBySkuIdListMethod(), responseObserver);
    }

    @java.lang.Override public final io.grpc.ServerServiceDefinition bindService() {
      return io.grpc.ServerServiceDefinition.builder(getServiceDescriptor())
          .addMethod(
            getGetSkuListBySkuIdListMethod(),
            io.grpc.stub.ServerCalls.asyncUnaryCall(
              new MethodHandlers<
                com.p7.architect.store.product.client.rpc.sku.SkuRequest,
                com.p7.architect.store.product.client.rpc.sku.SkuResponse>(
                  this, METHODID_GET_SKU_LIST_BY_SKU_ID_LIST)))
          .build();
    }
  }

  /**
   */
  public static final class SkuServiceStub extends io.grpc.stub.AbstractAsyncStub<SkuServiceStub> {
    private SkuServiceStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected SkuServiceStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new SkuServiceStub(channel, callOptions);
    }

    /**
     */
    public void getSkuListBySkuIdList(com.p7.architect.store.product.client.rpc.sku.SkuRequest request,
        io.grpc.stub.StreamObserver<com.p7.architect.store.product.client.rpc.sku.SkuResponse> responseObserver) {
      io.grpc.stub.ClientCalls.asyncUnaryCall(
          getChannel().newCall(getGetSkuListBySkuIdListMethod(), getCallOptions()), request, responseObserver);
    }
  }

  /**
   */
  public static final class SkuServiceBlockingStub extends io.grpc.stub.AbstractBlockingStub<SkuServiceBlockingStub> {
    private SkuServiceBlockingStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected SkuServiceBlockingStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new SkuServiceBlockingStub(channel, callOptions);
    }

    /**
     */
    public com.p7.architect.store.product.client.rpc.sku.SkuResponse getSkuListBySkuIdList(com.p7.architect.store.product.client.rpc.sku.SkuRequest request) {
      return io.grpc.stub.ClientCalls.blockingUnaryCall(
          getChannel(), getGetSkuListBySkuIdListMethod(), getCallOptions(), request);
    }
  }

  /**
   */
  public static final class SkuServiceFutureStub extends io.grpc.stub.AbstractFutureStub<SkuServiceFutureStub> {
    private SkuServiceFutureStub(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      super(channel, callOptions);
    }

    @java.lang.Override
    protected SkuServiceFutureStub build(
        io.grpc.Channel channel, io.grpc.CallOptions callOptions) {
      return new SkuServiceFutureStub(channel, callOptions);
    }

    /**
     */
    public com.google.common.util.concurrent.ListenableFuture<com.p7.architect.store.product.client.rpc.sku.SkuResponse> getSkuListBySkuIdList(
        com.p7.architect.store.product.client.rpc.sku.SkuRequest request) {
      return io.grpc.stub.ClientCalls.futureUnaryCall(
          getChannel().newCall(getGetSkuListBySkuIdListMethod(), getCallOptions()), request);
    }
  }

  private static final int METHODID_GET_SKU_LIST_BY_SKU_ID_LIST = 0;

  private static final class MethodHandlers<Req, Resp> implements
      io.grpc.stub.ServerCalls.UnaryMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ServerStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.ClientStreamingMethod<Req, Resp>,
      io.grpc.stub.ServerCalls.BidiStreamingMethod<Req, Resp> {
    private final SkuServiceImplBase serviceImpl;
    private final int methodId;

    MethodHandlers(SkuServiceImplBase serviceImpl, int methodId) {
      this.serviceImpl = serviceImpl;
      this.methodId = methodId;
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public void invoke(Req request, io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        case METHODID_GET_SKU_LIST_BY_SKU_ID_LIST:
          serviceImpl.getSkuListBySkuIdList((com.p7.architect.store.product.client.rpc.sku.SkuRequest) request,
              (io.grpc.stub.StreamObserver<com.p7.architect.store.product.client.rpc.sku.SkuResponse>) responseObserver);
          break;
        default:
          throw new AssertionError();
      }
    }

    @java.lang.Override
    @java.lang.SuppressWarnings("unchecked")
    public io.grpc.stub.StreamObserver<Req> invoke(
        io.grpc.stub.StreamObserver<Resp> responseObserver) {
      switch (methodId) {
        default:
          throw new AssertionError();
      }
    }
  }

  private static abstract class SkuServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoFileDescriptorSupplier, io.grpc.protobuf.ProtoServiceDescriptorSupplier {
    SkuServiceBaseDescriptorSupplier() {}

    @java.lang.Override
    public com.google.protobuf.Descriptors.FileDescriptor getFileDescriptor() {
      return com.p7.architect.store.product.client.rpc.sku.Sku.getDescriptor();
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.ServiceDescriptor getServiceDescriptor() {
      return getFileDescriptor().findServiceByName("SkuService");
    }
  }

  private static final class SkuServiceFileDescriptorSupplier
      extends SkuServiceBaseDescriptorSupplier {
    SkuServiceFileDescriptorSupplier() {}
  }

  private static final class SkuServiceMethodDescriptorSupplier
      extends SkuServiceBaseDescriptorSupplier
      implements io.grpc.protobuf.ProtoMethodDescriptorSupplier {
    private final String methodName;

    SkuServiceMethodDescriptorSupplier(String methodName) {
      this.methodName = methodName;
    }

    @java.lang.Override
    public com.google.protobuf.Descriptors.MethodDescriptor getMethodDescriptor() {
      return getServiceDescriptor().findMethodByName(methodName);
    }
  }

  private static volatile io.grpc.ServiceDescriptor serviceDescriptor;

  public static io.grpc.ServiceDescriptor getServiceDescriptor() {
    io.grpc.ServiceDescriptor result = serviceDescriptor;
    if (result == null) {
      synchronized (SkuServiceGrpc.class) {
        result = serviceDescriptor;
        if (result == null) {
          serviceDescriptor = result = io.grpc.ServiceDescriptor.newBuilder(SERVICE_NAME)
              .setSchemaDescriptor(new SkuServiceFileDescriptorSupplier())
              .addMethod(getGetSkuListBySkuIdListMethod())
              .build();
        }
      }
    }
    return result;
  }
}
