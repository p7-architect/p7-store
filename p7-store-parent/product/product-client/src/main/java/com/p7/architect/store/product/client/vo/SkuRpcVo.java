package com.p7.architect.store.product.client.vo;

import lombok.Data;

import java.io.Serializable;

@Data
public class SkuRpcVo implements Serializable {

    private static final long serialVersionUID = 1L;

    private Long skuId;

    private Long spuId;

    /**
     * 门店id
     */
    private Long storeId;

    /**
     * sku名称
     */
    private String skuName;

    /**
     * 默认图片
     */
    private String skuDefaultImg;

    /**
     * 标题
     */
    private String skuTitle;

    /**
     * 副标题
     */
    private String skuSubtitle;

    /**
     * 价格
     */
    private Integer price;

    /**
     * 币种
     */
    private String currency;

    /**
     * 库存
     */
    private Integer stockNum;
    /**
     * 销量
     */
    private Integer saleCount;

    /**
     * sku介绍描述
     */
    private String skuDesc;
}