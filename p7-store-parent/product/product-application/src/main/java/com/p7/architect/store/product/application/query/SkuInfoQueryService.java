package com.p7.architect.store.product.application.query;


import com.p7.architect.store.common.page.TableDataInfo;
import com.p7.architect.store.product.domain.data.vo.SkuVo;
import com.p7.architect.store.product.domain.model.product.SkuRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class SkuInfoQueryService {

    private final SkuRepository skuRepository;

    public TableDataInfo<SkuVo> queryWithSearchAndStoreId(SkuInfoQuery skuInfoQuery){
        TableDataInfo<SkuVo> skuVoTableDataInfo = skuRepository.searchSkuList(
                skuInfoQuery.storeId, skuInfoQuery.search,
                skuInfoQuery.getPageSize(), skuInfoQuery.getPageNum());
        return skuVoTableDataInfo;
    }
}
