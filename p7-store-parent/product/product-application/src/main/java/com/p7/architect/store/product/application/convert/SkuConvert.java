package com.p7.architect.store.product.application.convert;

import com.p7.architect.store.product.client.dto.SkuRpcDto;
import com.p7.architect.store.product.client.vo.SkuRpcVo;
import com.p7.architect.store.product.domain.data.dto.SkuDto;
import com.p7.architect.store.product.domain.data.vo.SkuVo;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * Sku转换
 *
 * @author dbc
 * @date 2022/06/12
 */
@Mapper
public interface SkuConvert extends BaseConvert<SkuRpcDto, SkuRpcVo, SkuDto, SkuVo> {
    SkuConvert IMPL = Mappers.getMapper(SkuConvert.class);
}
