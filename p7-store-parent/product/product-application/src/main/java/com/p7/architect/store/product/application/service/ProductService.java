package com.p7.architect.store.product.application.service;

import com.p7.architect.store.product.application.command.ListProductQuery;
import com.p7.architect.store.product.application.command.executor.ListProductQueryExecutor;
import com.p7.architect.store.product.domain.data.vo.SpuVo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ProductService {

    private final ListProductQueryExecutor listProductQueryExecutor;

    public List<SpuVo> executeListProductQuery(ListProductQuery listProductQuery) {
        return listProductQueryExecutor.execute(listProductQuery);
    }
}
