package com.p7.architect.store.product.application.convert;

import java.util.List;


public interface BaseConvert<RpcDto,RpcVo, Dto, Vo> {
    Dto rpcDto2Dto(RpcDto rpcDto);

    List<Dto> rpcDtoList2DtoList(List<RpcDto> rpcDtoList);

    RpcDto dto2RpcDto(Dto dto);

    List<RpcDto> dtoList2rpcDtoList(List<Dto> dtoList);

    Vo rpcVo2Vo(RpcVo rpcVo);

    List<Vo> rpcVoList2VoList(List<RpcVo> rpcVoList);

    RpcVo vo2RpcVo(Vo vo);

    List<RpcVo> voList2RpcVoList(List<Vo> voList);






    Vo dto2Vo(Dto dto);

    Dto vo2Dto(Vo vo);





}
