package com.p7.architect.store.product.application.query;

import cn.hutool.core.lang.Assert;
import com.p7.architect.store.product.domain.model.store.StoreId;

public class SkuInfoQuery {
    StoreId storeId;

    String search;

    Integer pageSize;

    Integer pageNum;

    public SkuInfoQuery(StoreId storeId, String search, Integer pageSize, Integer pageNum) {
        this.storeId = storeId;
        this.search = search;
        this.pageSize = pageSize;
        this.pageNum = pageNum;
    }

    public StoreId getStoreId() {
        return storeId;
    }

    public void setStoreId(StoreId storeId) {
        Assert.notNull(storeId, "storeId is require.");
        this.storeId = storeId;
    }

    public String getSearch() {
        return search;
    }

    public void setSearch(String search) {
        Assert.notNull(search, "search is require.");
        this.search = search;
    }

    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

    public Integer getPageNum() {
        return pageNum;
    }

    public void setPageNum(Integer pageNum) {
        this.pageNum = pageNum;
    }
}
