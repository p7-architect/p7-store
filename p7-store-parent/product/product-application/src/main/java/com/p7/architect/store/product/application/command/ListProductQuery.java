package com.p7.architect.store.product.application.command;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * desc:
 *
 * @ClassName: com.p7.architect.store.product.application.command.executor.ListProductQueryExecutor
 * @author: dengbicheng
 * @date: 2022-06-29 15:08
 * @version: v 1.0.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ListProductQuery {
    private Long storeId;
    private Integer page;
}
