package com.p7.architect.store.product.application.convert;

import com.p7.architect.store.product.client.dto.SpuRpcDto;
import com.p7.architect.store.product.client.vo.SpuRpcVo;
import com.p7.architect.store.product.domain.data.dto.SpuDto;
import com.p7.architect.store.product.domain.data.vo.SpuVo;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

/**
 * spu转换
 *
 * @author dbc
 * @date 2022/06/12
 */
@Mapper
public interface SpuConvert extends BaseConvert<SpuRpcDto, SpuRpcVo, SpuDto, SpuVo> {
	SpuConvert IMPL = Mappers.getMapper(SpuConvert.class);
}
