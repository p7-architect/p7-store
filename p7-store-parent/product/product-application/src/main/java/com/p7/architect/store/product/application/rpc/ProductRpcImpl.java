package com.p7.architect.store.product.application.rpc;

import com.p7.architect.store.product.application.command.ListProductQuery;
import com.p7.architect.store.product.application.convert.SpuConvert;
import com.p7.architect.store.product.application.service.ProductService;
import com.p7.architect.store.product.client.dto.ListProductRpcQuery;
import com.p7.architect.store.product.client.rpc.ProductRpc;
import com.p7.architect.store.product.client.vo.SpuRpcVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class ProductRpcImpl implements ProductRpc {

    @Autowired
    private ProductService productService;

    @Override
    public List<SpuRpcVo> getSpuList(ListProductRpcQuery listProductRpcQuery) {
        ListProductQuery listProductQuery = new ListProductQuery(listProductRpcQuery.getStoreId(), listProductRpcQuery.getPage());
        return SpuConvert.IMPL.voList2RpcVoList(productService.executeListProductQuery(listProductQuery));
    }
}
