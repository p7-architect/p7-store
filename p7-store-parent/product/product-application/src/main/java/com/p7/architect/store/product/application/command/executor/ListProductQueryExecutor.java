package com.p7.architect.store.product.application.command.executor;

import com.p7.architect.store.product.application.command.ListProductQuery;
import com.p7.architect.store.product.domain.data.vo.SpuVo;
import com.p7.architect.store.product.domain.model.product.SkuRepository;
import com.p7.architect.store.product.domain.model.product.SpuRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * desc: 产品列表查询执行器
 *
 * @ClassName: com.p7.architect.store.product.application.command.executor.ListProductQueryExecutor
 * @Author: dengbicheng
 * @CreateTime: 2022-06-29 17:37:33
 * @Version: v 1.0.0
 */
@Service
public class ListProductQueryExecutor {

    @Autowired
    private SpuRepository spuRepository;
    @Autowired
    private SkuRepository skuRepository;

    public List<SpuVo> execute(ListProductQuery query) {
        return null;
    }
}
