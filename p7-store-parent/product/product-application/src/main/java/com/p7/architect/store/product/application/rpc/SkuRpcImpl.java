package com.p7.architect.store.product.application.rpc;

import com.p7.architect.store.product.application.convert.SkuConvert;
import com.p7.architect.store.product.client.rpc.SkuRpc;
import com.p7.architect.store.product.client.vo.SkuRpcVo;
import com.p7.architect.store.product.domain.data.vo.SkuVo;
import com.p7.architect.store.product.domain.model.product.SkuRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class SkuRpcImpl implements SkuRpc {

    @Autowired
    private SkuRepository skuRepository;

    @Override
    public List<SkuRpcVo> getSkuListBySkuIdList(List<Long> skuIdList) {
        List<SkuVo> skuVoList = skuRepository.getSkuListBySkuIds(skuIdList);
        List<SkuRpcVo> skuRpcVos = SkuConvert.IMPL.voList2RpcVoList(skuVoList);
        return skuRpcVos;
    }


}
