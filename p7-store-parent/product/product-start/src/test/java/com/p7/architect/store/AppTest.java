package com.p7.architect.store;

import cn.hutool.json.JSONArray;
import cn.hutool.json.JSONUtil;
import com.p7.architect.store.product.application.convert.SkuConvert;
import com.p7.architect.store.product.client.vo.SkuRpcVo;
import com.p7.architect.store.product.domain.data.vo.SkuVo;
import com.p7.architect.store.product.start.ProductApplication;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = ProductApplication.class)
public class AppTest {

    @Test
    public void test(){
        String a ="[\n" +
                "    {\n" +
                "      \"storeId\": 123,\n" +
                "      \"skuName\": \"测试商品1\",\n" +
                "      \"skuDefaultImg\": null,\n" +
                "      \"skuTitle\": \"商品标题1\",\n" +
                "      \"skuSubtitle\": null,\n" +
                "      \"price\": 100,\n" +
                "      \"currency\": \"rmb\",\n" +
                "      \"saleCount\": null,\n" +
                "      \"skuDesc\": null\n" +
                "    },\n" +
                "    {\n" +
                "      \"storeId\": 123,\n" +
                "      \"skuName\": \"测试商品2\",\n" +
                "      \"skuDefaultImg\": null,\n" +
                "      \"skuTitle\": \"商品标题2\",\n" +
                "      \"skuSubtitle\": null,\n" +
                "      \"price\": 200,\n" +
                "      \"currency\": \"rmb\",\n" +
                "      \"saleCount\": null,\n" +
                "      \"skuDesc\": null\n" +
                "    }\n" +
                "  ]";
        JSONArray objects = JSONUtil.parseArray(a);
        List<SkuVo> skuVos = JSONUtil.toList(objects, SkuVo.class);
        System.out.println(skuVos);

        List<SkuRpcVo> skuRpcVos = SkuConvert.IMPL.voList2RpcVoList(skuVos);
        System.out.println(skuRpcVos);
    }
}

