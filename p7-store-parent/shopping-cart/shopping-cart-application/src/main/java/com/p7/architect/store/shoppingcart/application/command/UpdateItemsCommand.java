package com.p7.architect.store.shoppingcart.application.command;

import cn.hutool.core.lang.Validator;
import com.p7.architect.store.shoppingcart.domain.model.shoppingcart.ItemNum;
import com.p7.architect.store.shoppingcart.domain.model.shoppingcart.ShoppingCartId;
import com.p7.architect.store.shoppingcart.domain.model.value.SkuId;

public class UpdateItemsCommand {

    private ShoppingCartId shoppingCartId;

    private SkuId skuId;

    private ItemNum itemNum;

    public UpdateItemsCommand(ShoppingCartId shoppingCartId, SkuId skuId, ItemNum itemNum) {
        this.setShoppingCartId(shoppingCartId);
        this.setSkuId(skuId);
        this.setItemNum(itemNum);
    }

    public ShoppingCartId getShoppingCartId() {
        return shoppingCartId;
    }

    private void setShoppingCartId(ShoppingCartId shoppingCartId) {
        Validator.validateNotNull(shoppingCartId, "shoppingCartId is required");
        this.shoppingCartId = shoppingCartId;
    }

    public SkuId getSkuId() {
        return skuId;
    }

    private void setSkuId(SkuId skuId) {
        Validator.validateNotNull(skuId, "skuId is required");
        this.skuId = skuId;
    }

    public ItemNum getItemNum() {
        return itemNum;
    }

    private void setItemNum(ItemNum itemNum) {
        Validator.validateNotNull(itemNum, "itemNum is required");
        this.itemNum = itemNum;
    }
}
