package com.p7.architect.store.shoppingcart.application.command.dto;

import lombok.Data;

@Data
public class ShoppingItemDto {

    private Long skuId;
    private Long spuId;
    private String skuDefaultImg;
    private String skuTitle;
    private Integer num;
    private Integer price;

}
