package com.p7.architect.store.shoppingcart.application.query;

import cn.hutool.core.lang.Assert;
import com.p7.architect.store.shoppingcart.domain.model.shoppingcart.ShoppingCartId;
import com.p7.architect.store.shoppingcart.domain.model.value.StoreId;
import com.p7.architect.store.shoppingcart.domain.model.value.UserId;

public class ShoppingCartInfoQuery {

    private ShoppingCartId shoppingCartId;

    private UserId userId;

    private StoreId storeId;

    public ShoppingCartInfoQuery(ShoppingCartId shoppingCartId) {
        this.setShoppingCartId(shoppingCartId);
    }

    public ShoppingCartInfoQuery(UserId userId, StoreId storeId) {
        this.setUserId(userId);
        this.setStoreId(storeId);
    }

    public ShoppingCartId getShoppingCartId() {
        return shoppingCartId;
    }

    public UserId getUserId() {return userId; }

    public StoreId getStoreId() {return storeId; }

    public void setShoppingCartId(ShoppingCartId shoppingCartId) {
        Assert.notNull(shoppingCartId, "shoppingCartId is require.");
        this.shoppingCartId = shoppingCartId;
    }

    public void setStoreId(StoreId storeId) {
        Assert.notNull(storeId, "storeId is require.");
        this.storeId = storeId;
    }

    public void setUserId(UserId userId) {
        Assert.notNull(userId, "userId is require.");
        this.userId = userId;
    }
}
