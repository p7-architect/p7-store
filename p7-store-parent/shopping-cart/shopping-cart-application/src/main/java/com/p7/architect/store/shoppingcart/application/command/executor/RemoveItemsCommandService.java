package com.p7.architect.store.shoppingcart.application.command.executor;

import com.p7.architect.store.shoppingcart.application.command.RemoveItemsCommand;
import com.p7.architect.store.shoppingcart.domain.model.shoppingcart.ShoppingCart;
import com.p7.architect.store.shoppingcart.domain.model.shoppingcart.ShoppingCartId;
import com.p7.architect.store.shoppingcart.domain.model.shoppingcart.ShoppingCartRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class RemoveItemsCommandService {

    private final ShoppingCartRepository shoppingCartRepository;

    public ShoppingCartId execute(RemoveItemsCommand command) {
        ShoppingCart shoppingCart = shoppingCartRepository.find(command.getShoppingCartId());
        if (null == shoppingCart) {
            log.info("RemoveItemsCommandService#execute shoppingCart is null.");
            return null;
        }
        shoppingCart.removeItems(command.getSkuIdList());
        shoppingCartRepository.save(shoppingCart);
        return shoppingCart.getShoppingCartId();
    }
}
