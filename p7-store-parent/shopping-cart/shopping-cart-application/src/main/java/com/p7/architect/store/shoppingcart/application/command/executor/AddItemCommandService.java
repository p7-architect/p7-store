package com.p7.architect.store.shoppingcart.application.command.executor;

import com.p7.architect.store.shoppingcart.application.command.AddItemCommand;
import com.p7.architect.store.shoppingcart.domain.model.shoppingcart.ShoppingCart;
import com.p7.architect.store.shoppingcart.domain.model.shoppingcart.ShoppingCartId;
import com.p7.architect.store.shoppingcart.domain.model.shoppingcart.ShoppingCartRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class AddItemCommandService {

    private final ShoppingCartRepository shoppingCartRepository;

    public ShoppingCartId execute(AddItemCommand addItemCommand) {
        ShoppingCart shoppingCart = shoppingCartRepository.findByUserIdAndStoreId(addItemCommand.getUserId(), addItemCommand.getStoreId());
        shoppingCart.addItem(addItemCommand.getSkuId(), addItemCommand.getItemNum());
        return shoppingCartRepository.save(shoppingCart);
    }
}
