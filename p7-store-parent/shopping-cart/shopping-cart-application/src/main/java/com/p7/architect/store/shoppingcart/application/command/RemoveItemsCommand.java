package com.p7.architect.store.shoppingcart.application.command;

import cn.hutool.core.lang.Validator;
import com.p7.architect.store.shoppingcart.domain.model.shoppingcart.ShoppingCartId;
import com.p7.architect.store.shoppingcart.domain.model.value.SkuId;

import java.util.List;

public class RemoveItemsCommand {

    private ShoppingCartId shoppingCartId;

    private List<SkuId> skuIdList;

    public RemoveItemsCommand(ShoppingCartId shoppingCartId, List<SkuId> skuIdList) {
        this.setShoppingCartId(shoppingCartId);
        this.setSkuIdList(skuIdList);
    }

    public ShoppingCartId getShoppingCartId() {
        return shoppingCartId;
    }

    private void setShoppingCartId(ShoppingCartId shoppingCartId) {
        Validator.validateNotNull(shoppingCartId, "shoppingCartId is required");
        this.shoppingCartId = shoppingCartId;
    }

    public List<SkuId> getSkuIdList() {
        return skuIdList;
    }

    private void setSkuIdList(List<SkuId> skuIdList) {
        Validator.validateNotNull(skuIdList, "skuIdList is required");
        this.skuIdList = skuIdList;
    }
}
