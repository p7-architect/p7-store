package com.p7.architect.store.shoppingcart.application.command;

import cn.hutool.core.lang.Validator;
import com.p7.architect.store.shoppingcart.domain.model.shoppingcart.ItemNum;
import com.p7.architect.store.shoppingcart.domain.model.value.SkuId;
import com.p7.architect.store.shoppingcart.domain.model.value.StoreId;
import com.p7.architect.store.shoppingcart.domain.model.value.UserId;

public class AddItemCommand {

    private UserId userId;

    private StoreId storeId;

    private SkuId skuId;

    private ItemNum itemNum;

    public AddItemCommand(SkuId skuId, ItemNum itemNum, UserId userId, StoreId storeId) {
        this.setSkuId(skuId);
        this.setItemNum(itemNum);
        this.setUserId(userId);
        this.setStoreId(storeId);
    }

    public StoreId getStoreId() {
        return storeId;
    }

    public UserId getUserId() {
        return userId;
    }

    public SkuId getSkuId() {
        return skuId;
    }

    public ItemNum getItemNum() {
        return itemNum;
    }

    private void setStoreId(StoreId storeId) {
        Validator.validateNotNull(storeId, "storeId is required");
        this.storeId = storeId;
    }

    private void setUserId(UserId userId) {
        Validator.validateNotNull(userId, "userId is required");
        this.userId = userId;
    }

    public void setSkuId(SkuId skuId) {
        Validator.validateNotNull(skuId, "skuId must not null");
        this.skuId = skuId;
    }

    public void setItemNum(ItemNum itemNum) {
        Validator.validateNotNull(itemNum, "itemNum must not null");
        this.itemNum = itemNum;
    }
}
