package com.p7.architect.store.shoppingcart.application.command.dto;

import lombok.Data;

import java.util.List;

@Data
public class ShoppingCartDto {

    private Long shoppingCartId;
    private Long userId;
    private Long storeId;
    private Integer totalAmount;

    private List<ShoppingItemDto> shoppingItemDtoList;

}
