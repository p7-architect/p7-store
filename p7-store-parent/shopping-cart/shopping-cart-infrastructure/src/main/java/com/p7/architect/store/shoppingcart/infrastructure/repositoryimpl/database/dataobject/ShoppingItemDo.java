package com.p7.architect.store.shoppingcart.infrastructure.repositoryimpl.database.dataobject;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@TableName("oms_shopping_item")
@Builder
public class ShoppingItemDo {

    @TableId(type = IdType.AUTO)
    private Long itemId;

    private Long shoppingCartId;

    private Long skuId;

    private Integer num;

    private Long createdBy;

    private Long updatedBy;

    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createdTime;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updatedTime;
}
