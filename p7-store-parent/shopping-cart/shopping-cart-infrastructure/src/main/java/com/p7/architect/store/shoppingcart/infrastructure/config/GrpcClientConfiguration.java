package com.p7.architect.store.shoppingcart.infrastructure.config;

import com.p7.architect.store.product.client.rpc.sku.SkuServiceGrpc;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.util.concurrent.TimeUnit;

@Slf4j
@Component
public class GrpcClientConfiguration {

    /**
     * gRPC Server的地址
     */
    @Value("${grpc.server-host}")
    private String host;

    /**
     * gRPC Server的端口
     */
    @Value("${grpc.server-port}")
    private int port;

    private ManagedChannel channel;

    SkuServiceGrpc.SkuServiceBlockingStub stub;

    public void start() {
        // 开启channel
        channel = ManagedChannelBuilder.forAddress(host, port).usePlaintext().build();

        // 通过channel获取到服务端的stub
        stub = SkuServiceGrpc.newBlockingStub(channel);
        log.info("gRPC client started, server address: {}:{}", host, port);
    }

    public void shutdown() throws InterruptedException {
        // 调用shutdown方法后等待1秒关闭channel
        channel.shutdown().awaitTermination(1, TimeUnit.SECONDS);
        log.info("gRPC client shut down successfully.");
    }

    public SkuServiceGrpc.SkuServiceBlockingStub getStub() {
        return this.stub;
    }
}