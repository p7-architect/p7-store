package com.p7.architect.store.shoppingcart.infrastructure.util;

import com.p7.architect.store.shoppingcart.domain.shared.Aggregate;
import com.p7.architect.store.shoppingcart.domain.shared.ValueObject;
import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.Field;

@Slf4j
public class ReflectionUtils {

    public static <T extends Aggregate<ID>, ID extends ValueObject> void writeField(
            String fieldName, T aggregate, ID id) {
        Class<? extends Aggregate> aClass = aggregate.getClass();
        try {
            Field field = aClass.getDeclaredField(fieldName);
            field.setAccessible(true);
            field.set(aggregate, id);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            log.error("ReflectionUtils writeField failure, error: {}", e);
        }
    }
}
