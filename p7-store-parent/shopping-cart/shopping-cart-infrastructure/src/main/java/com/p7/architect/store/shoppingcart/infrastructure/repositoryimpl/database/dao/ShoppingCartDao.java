package com.p7.architect.store.shoppingcart.infrastructure.repositoryimpl.database.dao;

import com.p7.architect.store.shoppingcart.infrastructure.config.BaseDao;
import com.p7.architect.store.shoppingcart.infrastructure.repositoryimpl.database.dataobject.ShoppingCartDo;
import com.p7.architect.store.shoppingcart.infrastructure.repositoryimpl.database.mapper.ShoppingCartMapper;
import org.springframework.stereotype.Repository;

@Repository
public class ShoppingCartDao extends BaseDao<ShoppingCartMapper, ShoppingCartDo> {

}
