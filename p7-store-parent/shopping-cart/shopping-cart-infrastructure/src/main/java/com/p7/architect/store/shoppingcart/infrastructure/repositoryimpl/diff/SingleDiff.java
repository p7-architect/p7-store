package com.p7.architect.store.shoppingcart.infrastructure.repositoryimpl.diff;

public class SingleDiff implements Diff {

    private Object oldValue;
    private Object newValue;

    /**
     * {@link DiffType}
     */
    private int diffType;

    public SingleDiff() {
    }

    public SingleDiff(Object oldValue, Object newValue) {
        this.oldValue = oldValue;
        this.newValue = newValue;
    }

    public SingleDiff(Object oldValue, Object newValue, int diffType) {
        this.oldValue = oldValue;
        this.newValue = newValue;
        this.diffType = diffType;
    }

    public Object getOldValue() {
        return oldValue;
    }

    public Object getNewValue() {
        return newValue;
    }

    public int getDiffType() {
        return diffType;
    }

    public void setDiffType(int diffType) {
        this.diffType = diffType;
    }
}
