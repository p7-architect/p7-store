package com.p7.architect.store.shoppingcart.infrastructure.repositoryimpl;

import cn.hutool.core.collection.CollectionUtil;
import com.p7.architect.store.product.client.rpc.sku.SkuRequest;
import com.p7.architect.store.product.client.rpc.sku.SkuResponse;
import com.p7.architect.store.product.client.rpc.sku.SkuVo;
import com.p7.architect.store.shoppingcart.infrastructure.config.GrpcClientConfiguration;
import com.p7.architect.store.shoppingcart.infrastructure.repositoryimpl.vo.Sku;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Slf4j
@Component
@RequiredArgsConstructor
public class SkuRepositoryImpl {

    private final GrpcClientConfiguration configuration;

    public List<Sku> findSkuListBySkuIdList(List<Long> skuIds) {
        SkuResponse skuResponse = configuration.getStub().getSkuListBySkuIdList(SkuRequest.newBuilder().addAllSkuIdList(skuIds).build());
        SkuResponse.Data data = skuResponse.getData();
        List<SkuVo> skuVoList = data.getSkuVoList();
        if (CollectionUtil.isEmpty(skuVoList)) {
            return Collections.emptyList();
        }

        List<Sku> skus = new ArrayList<>();
        for (SkuVo vo : skuVoList) {
            skus.add(new Sku(vo.getSkuId(), vo.getSpuId(), vo.getSkuTitle(),
                    vo.getPrice(), vo.getSkuDefaultImg()));
        }
        return skus;
    }
}
