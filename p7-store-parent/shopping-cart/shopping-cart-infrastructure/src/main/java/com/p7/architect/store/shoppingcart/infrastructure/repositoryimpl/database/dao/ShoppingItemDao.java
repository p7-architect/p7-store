package com.p7.architect.store.shoppingcart.infrastructure.repositoryimpl.database.dao;

import com.p7.architect.store.shoppingcart.infrastructure.config.BaseDao;
import com.p7.architect.store.shoppingcart.infrastructure.repositoryimpl.database.dataobject.ShoppingItemDo;
import com.p7.architect.store.shoppingcart.infrastructure.repositoryimpl.database.mapper.ShoppingItemMapper;
import org.springframework.stereotype.Repository;

@Repository
public class ShoppingItemDao extends BaseDao<ShoppingItemMapper, ShoppingItemDo> {

}
