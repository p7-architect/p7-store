package com.p7.architect.store.shoppingcart.infrastructure.repositoryimpl.diff;

public enum DiffType {

    ADDED(1),
    MODIFIED(2),
    DELETED(0);

    private final int value;

    DiffType(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }
}
