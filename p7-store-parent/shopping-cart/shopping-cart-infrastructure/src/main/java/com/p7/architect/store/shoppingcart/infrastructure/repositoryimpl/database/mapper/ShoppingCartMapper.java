package com.p7.architect.store.shoppingcart.infrastructure.repositoryimpl.database.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.p7.architect.store.shoppingcart.infrastructure.repositoryimpl.database.dataobject.ShoppingCartDo;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ShoppingCartMapper extends BaseMapper<ShoppingCartDo> {

}
