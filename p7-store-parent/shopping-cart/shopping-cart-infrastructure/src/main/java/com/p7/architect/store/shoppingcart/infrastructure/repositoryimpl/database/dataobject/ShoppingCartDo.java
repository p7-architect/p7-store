package com.p7.architect.store.shoppingcart.infrastructure.repositoryimpl.database.dataobject;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@TableName("oms_shopping_cart")
@Builder
public class ShoppingCartDo {

    @TableId(type = IdType.AUTO)
    private Long shoppingCartId;

    private Long userId;

    private Long storeId;

    private Long createdBy;

    private Long updatedBy;

    @TableField(fill = FieldFill.INSERT)
    private LocalDateTime createdTime;

    @TableField(fill = FieldFill.INSERT_UPDATE)
    private LocalDateTime updatedTime;

}
