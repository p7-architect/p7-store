package com.p7.architect.store.shoppingcart.infrastructure.converter;

import com.p7.architect.store.shoppingcart.domain.model.shoppingcart.ShoppingCart;
import com.p7.architect.store.shoppingcart.domain.model.shoppingcart.ShoppingCartId;
import com.p7.architect.store.shoppingcart.domain.model.value.StoreId;
import com.p7.architect.store.shoppingcart.domain.model.value.UserId;
import com.p7.architect.store.shoppingcart.infrastructure.repositoryimpl.database.dataobject.ShoppingCartDo;
import com.p7.architect.store.shoppingcart.infrastructure.repositoryimpl.database.dataobject.ShoppingItemDo;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@RequiredArgsConstructor
public class ShoppingCartDoConverter {

    final ShoppingItemDoConverter shoppingItemDOConverter;

    public ShoppingCartDo convert(ShoppingCart shoppingCart) {
        return ShoppingCartDo.builder()
                .shoppingCartId(shoppingCart.getShoppingCartId() == null || shoppingCart.getShoppingCartId().getId() == null ?
                        null : shoppingCart.getShoppingCartId().getId())
                .userId(shoppingCart.getUserId().getId())
                .storeId(shoppingCart.getStoreId().getId())
                .createdBy(shoppingCart.getUserId().getId())
                .updatedBy(shoppingCart.getUserId().getId()).build();
    }

    public ShoppingCart convert(ShoppingCartDo shoppingCartDO, List<ShoppingItemDo> shoppingItemDOList) {
        return ShoppingCart.builder()
                .shoppingCartId(new ShoppingCartId(shoppingCartDO.getShoppingCartId()))
                .userId(new UserId(shoppingCartDO.getUserId()))
                .storeId(new StoreId(shoppingCartDO.getStoreId()))
                .items(shoppingItemDOConverter.convertItemList(shoppingItemDOList))
                .build();
    }
}
