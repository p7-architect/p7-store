package com.p7.architect.store.shoppingcart.infrastructure.converter;

import com.p7.architect.store.shoppingcart.domain.model.shoppingcart.ItemNum;
import com.p7.architect.store.shoppingcart.domain.model.shoppingcart.ShoppingCart;
import com.p7.architect.store.shoppingcart.domain.model.shoppingcart.ShoppingItem;
import com.p7.architect.store.shoppingcart.domain.model.value.SkuId;
import com.p7.architect.store.shoppingcart.infrastructure.repositoryimpl.database.dataobject.ShoppingItemDo;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class ShoppingItemDoConverter {

    public ShoppingItem convert(ShoppingItemDo shoppingItemDO) {
        return ShoppingItem.builder()
                .skuId(new SkuId(shoppingItemDO.getSkuId()))
                .itemNum(new ItemNum(shoppingItemDO.getNum()))
                .build();
    }

    public List<ShoppingItem> convertItemList(List<ShoppingItemDo> shoppingItemDoList) {
        return shoppingItemDoList.stream().map(this::convert).collect(Collectors.toList());
    }

    public ShoppingItemDo convert(ShoppingItem shoppingItem, ShoppingCart shoppingCart) {
        return ShoppingItemDo.builder()
                .shoppingCartId(shoppingCart.getShoppingCartId() == null ? null : shoppingCart.getShoppingCartId().getId())
                .skuId(shoppingItem.getSkuId().getId())
                .num(shoppingItem.getItemNum().getNum())
                .createdBy(shoppingCart.getUserId().getId())
                .updatedBy(shoppingCart.getUserId().getId())
                .build();
    }
}
