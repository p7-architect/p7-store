package com.p7.architect.store.shoppingcart.infrastructure.repositoryimpl.vo;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Sku {

    private Long skuId;
    private Long spuId;
    private String skuTitle;
    private Integer price;
    private String skuDefaultImg;

}
