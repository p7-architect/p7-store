package com.p7.architect.store.shoppingcart.infrastructure.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import(value = {MybatisPlusConfig.class})
public class CommonAutoConfiguration {
}