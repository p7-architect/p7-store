package com.p7.architect.store.shoppingcart.infrastructure.repositoryimpl.diff;

import com.p7.architect.store.shoppingcart.domain.shared.Aggregate;
import com.p7.architect.store.shoppingcart.domain.shared.ValueObject;

public class AggregateManagerFactory {

    public static <ID extends ValueObject, T extends Aggregate<ID>>
     AggregateManager<T, ID> newInstance(Class<T> targetClass) {
        return new ThreadLocalAggregateManager<>(targetClass);
    }
}
