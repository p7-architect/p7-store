package com.p7.architect.store.shoppingcart.infrastructure.repositoryimpl.diff;

public interface Diff {

    Object getOldValue();

    Object getNewValue();

    int getDiffType();
}
