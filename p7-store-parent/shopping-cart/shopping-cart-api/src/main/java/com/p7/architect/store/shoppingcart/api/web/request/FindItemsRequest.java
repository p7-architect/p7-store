package com.p7.architect.store.shoppingcart.api.web.request;

import lombok.Data;

@Data
public class FindItemsRequest {

    private Long userId;

    private Long storeId;
}
