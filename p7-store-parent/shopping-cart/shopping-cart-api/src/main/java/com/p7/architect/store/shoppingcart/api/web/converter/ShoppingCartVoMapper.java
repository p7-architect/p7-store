package com.p7.architect.store.shoppingcart.api.web.converter;

import com.p7.architect.store.shoppingcart.api.web.response.ShoppingCartResponse;
import com.p7.architect.store.shoppingcart.application.command.dto.ShoppingCartDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.control.DeepClone;
import org.mapstruct.factory.Mappers;

@Mapper
public interface ShoppingCartVoMapper {

    ShoppingCartVoMapper INSTANCE = Mappers.getMapper(ShoppingCartVoMapper.class);

    @Mapping(target = "shoppingItemResponseList", source = "shoppingItemDtoList", mappingControl = DeepClone.class)
    ShoppingCartResponse toShoppingCartResponse(ShoppingCartDto shoppingCartDTO);
}
