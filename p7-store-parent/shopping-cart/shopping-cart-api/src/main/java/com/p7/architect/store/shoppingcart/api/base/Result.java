package com.p7.architect.store.shoppingcart.api.base;

import lombok.Data;

@Data
public class Result<T> {

    private int code;
    private String msg;
    private T data;

    /**
     * 成功
     */
    public static <T> Result<T> success(T data) {
        return new Result<>(data);
    }

    public Result(int code, String msg, T data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }

    /**
     * 成功的构造函数
     *
     * @param data
     */
    private Result(T data) {
        this.code = 200;
        this.msg = "SUCCESS";
        this.data = data;
    }

    /**
     * 失败
     */
    public static <T> Result<T> error(Proto proto) {
        return new Result<>(proto);
    }

    /**
     * 失败的构造函数
     *
     * @param proto
     */
    private Result(Proto proto) {
        if (proto != null) {
            this.code = proto.getCode();
            this.msg = proto.getMsg();
        }
    }
}
