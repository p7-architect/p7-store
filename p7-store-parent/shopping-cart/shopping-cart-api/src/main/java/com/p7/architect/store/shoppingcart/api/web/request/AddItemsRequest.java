package com.p7.architect.store.shoppingcart.api.web.request;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class AddItemsRequest {

    @NotNull(message = "skuId不能为空")
    Long skuId;

    @NotNull(message = "count不能为空")
    Integer count;

    @NotNull(message = "userId不能为空")
    Long userId;

    @NotNull(message = "storeId不能为空")
    Long storeId;
}
