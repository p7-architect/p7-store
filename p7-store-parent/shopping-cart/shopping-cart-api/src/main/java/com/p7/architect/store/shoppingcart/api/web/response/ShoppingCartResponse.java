package com.p7.architect.store.shoppingcart.api.web.response;

import lombok.Data;

import java.util.List;

@Data
public class ShoppingCartResponse {

    private Long shoppingCartId;
    private Long userId;
    private Long storeId;
    private Integer totalAmount;

    private List<ShoppingItemResponse> shoppingItemResponseList;
}
