package com.p7.architect.store.shoppingcart.api.web.request;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class RemoveItemsRequest {

    @NotNull(message = "shoppingCartId不能为空")
    Long shoppingCartId;

    @NotNull(message = "skuIdList不能为空")
    private List<Long> skuIdList;
}
