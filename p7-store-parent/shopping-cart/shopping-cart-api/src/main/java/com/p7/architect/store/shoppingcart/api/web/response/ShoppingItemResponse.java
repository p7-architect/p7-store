package com.p7.architect.store.shoppingcart.api.web.response;

import lombok.Data;

@Data
public class ShoppingItemResponse {

    private Long skuId;
    private Long spuId;
    private String skuDefaultImg;
    private String skuTitle;
    private Integer num;
    private Integer price;

}
