package com.p7.architect.store.shoppingcart.api.web.request;

import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Data
public class UpdateItemsRequest {

    @NotNull(message = "shoppingCartId不能为空")
    Long shoppingCartId;

    @NotNull(message = "skuId不能为空")
    Long skuId;

    @NotNull(message = "count不能为空")
    @Min(value = 1, message = "count数量必须大于0")
    Integer count;

}
