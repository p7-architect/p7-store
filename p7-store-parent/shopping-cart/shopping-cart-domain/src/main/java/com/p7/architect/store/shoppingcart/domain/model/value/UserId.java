package com.p7.architect.store.shoppingcart.domain.model.value;

import cn.hutool.core.lang.Validator;
import com.p7.architect.store.shoppingcart.domain.shared.ValueObject;

public class UserId implements ValueObject<UserId> {

    private Long id;

    public UserId(Long id) {
        this.setId(id);
    }

    private void setId(Long id) {
        Validator.validateNotNull(id, "user id is required");
        this.id = id;
    }

    public Long getId() {
        return this.id;
    }

    @Override
    public boolean sameValueAs(UserId userId) {
        return this.id.equals(userId.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        UserId userId = (UserId) o;

        return sameValueAs(userId);
    }
}
