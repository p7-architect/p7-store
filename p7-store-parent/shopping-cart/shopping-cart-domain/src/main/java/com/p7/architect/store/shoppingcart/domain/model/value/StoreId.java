package com.p7.architect.store.shoppingcart.domain.model.value;

import cn.hutool.core.lang.Validator;
import com.p7.architect.store.shoppingcart.domain.shared.ValueObject;

public class StoreId implements ValueObject<StoreId> {

    private Long id;

    public StoreId(Long id) {
        this.setId(id);
    }

    public Long getId() {
        return this.id;
    }

    private void setId(Long id) {
        Validator.validateNotNull(id, "store id is required");
        this.id = id;
    }

    @Override
    public boolean sameValueAs(StoreId storeId) {
        return this.id.equals(storeId.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        StoreId storeId = (StoreId) o;

        return sameValueAs(storeId);
    }
}
