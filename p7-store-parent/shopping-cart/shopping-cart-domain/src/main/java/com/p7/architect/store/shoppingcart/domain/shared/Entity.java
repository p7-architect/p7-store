package com.p7.architect.store.shoppingcart.domain.shared;

public interface Entity<ID extends ValueObject> extends Identifiable<ID> {

}
