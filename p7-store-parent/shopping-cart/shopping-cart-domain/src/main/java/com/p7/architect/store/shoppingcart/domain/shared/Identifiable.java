package com.p7.architect.store.shoppingcart.domain.shared;

import java.io.Serializable;

public interface Identifiable<ID extends ValueObject> extends Serializable {

    ID getId();
}
