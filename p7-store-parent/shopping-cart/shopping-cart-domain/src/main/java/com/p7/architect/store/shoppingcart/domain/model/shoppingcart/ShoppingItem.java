package com.p7.architect.store.shoppingcart.domain.model.shoppingcart;

import cn.hutool.core.lang.Validator;
import com.p7.architect.store.shoppingcart.domain.model.value.SkuId;
import com.p7.architect.store.shoppingcart.domain.shared.Entity;
import lombok.Builder;

@Builder
public class ShoppingItem implements Entity<SkuId> {

    private SkuId skuId;

    private ItemNum itemNum;

    public ShoppingItem(SkuId skuId, ItemNum itemNum) {
        this.setSkuId(skuId);
        this.setNum(itemNum);
    }

    public SkuId getSkuId() {
        return skuId;
    }

    public ItemNum getItemNum() {
        return itemNum;
    }

    private void setSkuId(SkuId skuId) {
        Validator.validateNotNull(skuId, "skuId is required");
        this.skuId = skuId;
    }

    public void updateNum(ItemNum updateNum) {
        itemNum = updateNum;
    }

    private void setNum(ItemNum itemNum) {
        Validator.validateNotNull(itemNum, "itemNum is required");
        this.itemNum = itemNum;
    }

    public void addNum(ItemNum addNum) {
        itemNum = itemNum.addNum(addNum);
    }

    @Override
    public SkuId getId() {
        return this.skuId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ShoppingItem shoppingItem = (ShoppingItem) o;

        return skuId.sameValueAs(shoppingItem.skuId);
    }

    @Override
    public int hashCode() {
        return skuId.hashCode();
    }

}
