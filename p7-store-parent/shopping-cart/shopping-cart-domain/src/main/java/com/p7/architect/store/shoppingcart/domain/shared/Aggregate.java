package com.p7.architect.store.shoppingcart.domain.shared;

/**
 * 聚合根
 */
public interface Aggregate<ID extends ValueObject> extends Entity<ID> {

}
