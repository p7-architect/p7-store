package com.p7.architect.store.shoppingcart.domain.model.value;

import cn.hutool.core.lang.Validator;
import com.p7.architect.store.shoppingcart.domain.shared.ValueObject;

public class SkuId implements ValueObject<SkuId> {

    private Long id;

    public SkuId(Long id) {
        this.setId(id);
    }

    private void setId(Long id) {
        Validator.validateNotNull(id, "sku id is required");
        this.id = id;
    }

    public Long getId() {
        return this.id;
    }

    @Override
    public boolean sameValueAs(SkuId skuId) {
        return this.id.equals(skuId.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        SkuId skuId = (SkuId) o;

        return sameValueAs(skuId);
    }
}
