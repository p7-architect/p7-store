package com.p7.architect.store.shoppingcart.domain.model.shoppingcart;

import cn.hutool.core.lang.Validator;
import com.p7.architect.store.shoppingcart.domain.shared.ValueObject;

public class ItemNum implements ValueObject<ItemNum> {

    private Integer num;

    public ItemNum(Integer num) {
        this.setNum(num);
    }

    public ItemNum addNum(ItemNum addNum) {
        Validator.validateNotNull(addNum, "addNum is required");
        return new ItemNum(this.num + addNum.getNum());
    }

    private void setNum(Integer num) {
        Validator.validateNotNull(num, "num is required");
        if (num < 0) {
            throw new IllegalArgumentException("num must greater than 0");
        }
        this.num = num;
    }

    public Integer getNum() {
        return this.num;
    }

    @Override
    public boolean sameValueAs(ItemNum itemNum) {
        return this.num.equals(itemNum.num);
    }

    @Override
    public int hashCode() {
        return num.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ItemNum itemNum = (ItemNum) o;

        return sameValueAs(itemNum);
    }
}
