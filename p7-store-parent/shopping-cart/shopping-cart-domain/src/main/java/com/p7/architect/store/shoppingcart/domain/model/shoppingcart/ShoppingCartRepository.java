package com.p7.architect.store.shoppingcart.domain.model.shoppingcart;

import com.p7.architect.store.shoppingcart.domain.model.value.StoreId;
import com.p7.architect.store.shoppingcart.domain.model.value.UserId;
import com.p7.architect.store.shoppingcart.domain.shared.Repository;

public interface ShoppingCartRepository extends Repository<ShoppingCart, ShoppingCartId> {

    ShoppingCart findByUserIdAndStoreId(UserId userId, StoreId storeId);

    ShoppingCartId save(ShoppingCart shoppingCart);

    ShoppingCart findById(ShoppingCartId shoppingCartId);

    void remove(ShoppingCart shoppingCart);
}
