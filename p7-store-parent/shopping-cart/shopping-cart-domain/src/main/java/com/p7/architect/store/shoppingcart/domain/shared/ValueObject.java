package com.p7.architect.store.shoppingcart.domain.shared;

import java.io.Serializable;

public interface ValueObject<T> extends Serializable {

    default boolean sameValueAs(T other) {
        return this.equals(other);
    }

}
