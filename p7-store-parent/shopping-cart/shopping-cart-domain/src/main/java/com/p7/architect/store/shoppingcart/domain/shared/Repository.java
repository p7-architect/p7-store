package com.p7.architect.store.shoppingcart.domain.shared;

import org.jetbrains.annotations.NotNull;

public interface Repository<T extends Aggregate<ID>, ID extends ValueObject> {

    void attach(@NotNull T aggregate);

    void detach(@NotNull T aggregate);

    T find(@NotNull ID id);

    void remove(@NotNull T aggregate);

    ID save(@NotNull T aggregate);
}
