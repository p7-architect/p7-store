package com.p7.architect.store.shoppingcart.domain.model.shoppingcart;

import cn.hutool.core.lang.Validator;
import com.p7.architect.store.shoppingcart.domain.shared.ValueObject;

public class ShoppingCartId implements ValueObject<ShoppingCartId> {

    private Long id;

    public ShoppingCartId(Long id) {
        this.setId(id);
    }

    private void setId(Long id) {
        Validator.validateNotNull(id, "shoppingCartId is required");
        this.id = id;
    }

    public Long getId() {
        return this.id;
    }

    @Override
    public boolean sameValueAs(ShoppingCartId shoppingCartId) {
        return this.id.equals(shoppingCartId.id);
    }

    @Override
    public int hashCode() {
        return id.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ShoppingCartId shoppingCartId = (ShoppingCartId) o;

        return sameValueAs(shoppingCartId);
    }
}
