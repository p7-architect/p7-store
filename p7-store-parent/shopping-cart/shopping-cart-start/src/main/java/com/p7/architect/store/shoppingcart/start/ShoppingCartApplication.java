package com.p7.architect.store.shoppingcart.start;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = {"com.p7.architect.store.shoppingcart"})
@MapperScan(basePackages = "com.p7.architect.store.shoppingcart.infrastructure.repositoryimpl.database")
public class ShoppingCartApplication {

    public static void main(String[] args) {
        SpringApplication.run(ShoppingCartApplication.class, args);
    }
}
