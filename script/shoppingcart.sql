-- MySQL dump 10.13  Distrib 5.7.31, for macos10.14 (x86_64)
--
-- Host: rm-bp1lu777b9843uw9y1o.mysql.rds.aliyuncs.com    Database: db_p7stroe_shopping_cart
-- ------------------------------------------------------
-- Server version	8.0.18

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
SET @MYSQLDUMP_TEMP_LOG_BIN = @@SESSION.SQL_LOG_BIN;
SET @@SESSION.SQL_LOG_BIN= 0;

--
-- GTID state at the beginning of the backup 
--

SET @@GLOBAL.GTID_PURGED='22be4b30-41cf-11ec-852d-00163e0a60b2:1-1316585';

--
-- Current Database: `db_p7stroe_shopping_cart`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `db_p7stroe_shopping_cart` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;

USE `db_p7stroe_shopping_cart`;

--
-- Table structure for table `oms_shopping_cart`
--

DROP TABLE IF EXISTS `oms_shopping_cart`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oms_shopping_cart` (
  `shopping_cart_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '购物车id',
  `user_id` bigint(20) DEFAULT NULL COMMENT '用户id',
  `store_id` bigint(20) DEFAULT NULL COMMENT '门店id',
  `created_by` bigint(20) DEFAULT NULL COMMENT '创建人',
  `created_time` datetime DEFAULT NULL COMMENT '创建时间',
  `updated_by` bigint(20) DEFAULT NULL COMMENT '更新人',
  `updated_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`shopping_cart_id`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='购物车表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oms_shopping_cart`
--

LOCK TABLES `oms_shopping_cart` WRITE;
/*!40000 ALTER TABLE `oms_shopping_cart` DISABLE KEYS */;
INSERT INTO `oms_shopping_cart` VALUES (22,1,123,1,'2022-07-24 21:04:14',1,'2022-08-06 17:32:40'),(23,100,100,100,'2022-07-27 13:36:09',100,'2022-08-01 15:16:51');
/*!40000 ALTER TABLE `oms_shopping_cart` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `oms_shopping_item`
--

DROP TABLE IF EXISTS `oms_shopping_item`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `oms_shopping_item` (
  `item_id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '购物车明细ID',
  `shopping_cart_id` bigint(20) DEFAULT NULL COMMENT '购物车id',
  `sku_id` bigint(20) DEFAULT NULL COMMENT '库存量单位id',
  `num` int(11) DEFAULT NULL COMMENT '数量',
  `created_by` bigint(20) DEFAULT NULL COMMENT '创建人',
  `created_time` datetime DEFAULT NULL COMMENT '创建时间',
  `updated_by` bigint(20) DEFAULT NULL COMMENT '更新人',
  `updated_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`item_id`)
) ENGINE=InnoDB AUTO_INCREMENT=134 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci COMMENT='购物车明细表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `oms_shopping_item`
--

LOCK TABLES `oms_shopping_item` WRITE;
/*!40000 ALTER TABLE `oms_shopping_item` DISABLE KEYS */;
INSERT INTO `oms_shopping_item` VALUES (100,22,4,13,1,'2022-08-01 17:16:28',1,'2022-08-01 19:06:51'),(123,22,2,11,1,'2022-08-01 18:43:08',1,'2022-08-01 19:06:50'),(125,22,3,9,1,'2022-08-01 18:56:42',1,'2022-08-01 19:00:23'),(127,22,1,3,1,'2022-08-01 18:59:11',1,'2022-08-01 18:59:11'),(132,22,6,1,1,'2022-08-06 17:28:59',1,'2022-08-06 17:32:33'),(133,22,6,2,1,'2022-08-06 17:32:40',1,'2022-08-06 17:32:40');
/*!40000 ALTER TABLE `oms_shopping_item` ENABLE KEYS */;
UNLOCK TABLES;
SET @@SESSION.SQL_LOG_BIN = @MYSQLDUMP_TEMP_LOG_BIN;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-08-06 20:55:19
