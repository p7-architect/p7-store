-- MySQL dump 10.13  Distrib 5.7.31, for macos10.14 (x86_64)
--
-- Host: rm-bp1lu777b9843uw9y1o.mysql.rds.aliyuncs.com    Database: db_p7store
-- ------------------------------------------------------
-- Server version	8.0.18

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
SET @MYSQLDUMP_TEMP_LOG_BIN = @@SESSION.SQL_LOG_BIN;
SET @@SESSION.SQL_LOG_BIN= 0;

--
-- GTID state at the beginning of the backup 
--

SET @@GLOBAL.GTID_PURGED='22be4b30-41cf-11ec-852d-00163e0a60b2:1-1316578';

--
-- Current Database: `db_p7store_product`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `db_p7store_product` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;

USE `db_p7store_product`;

--
-- Table structure for table `pms_sku`
--

DROP TABLE IF EXISTS `pms_sku`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pms_sku` (
  `sku_id` bigint(20) NOT NULL COMMENT 'skuId',
  `spu_id` bigint(20) DEFAULT NULL COMMENT 'spuId',
  `store_id` bigint(20) DEFAULT NULL COMMENT '门店id',
  `sku_name` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'sku名称',
  `sku_default_img` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '默认图片',
  `sku_title` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '标题',
  `sku_subtitle` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '副标题',
  `price` int(11) DEFAULT NULL COMMENT '价格',
  `currency` varchar(16) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '币种',
  `stock_num` int(11) DEFAULT NULL COMMENT '库存量',
  `sale_count` int(11) DEFAULT NULL COMMENT '销量',
  `sku_desc` varchar(900) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT 'sku介绍描述',
  `create_by` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`sku_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='库存量单位表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pms_sku`
--

LOCK TABLES `pms_sku` WRITE;
/*!40000 ALTER TABLE `pms_sku` DISABLE KEYS */;
INSERT INTO `pms_sku` VALUES (1,1,123,'Apple MacBook Pro 14英寸 M1 Pro芯片 8G 500G深空灰','https://img12.360buyimg.com/n7/jfs/t1/133389/8/28026/35103/6299f47aE65b351e2/e25162f3d05bc5a6.jpg','Apple MacBook Pro 14英寸 M1 Pro芯片 8G 500G深空灰笔记本电脑 MKGQ3CH/A','test',1599900,'rmb',1000,NULL,NULL,NULL,'2022-07-13 22:29:55',NULL,NULL),(2,1,123,'Apple MacBook Pro 14英寸 M1 Pro芯片 8G 500G深空灰','https://img12.360buyimg.com/n7/jfs/t1/133389/8/28026/35103/6299f47aE65b351e2/e25162f3d05bc5a6.jpg','Apple MacBook Pro 14英寸 M1 Pro芯片 8G 500G深空灰笔记本电脑 MKGQ3CH/A','test',1599900,'rmb',2000,NULL,NULL,NULL,NULL,NULL,NULL),(3,1,123,'Apple MacBook Pro 14英寸 M1 Pro芯片 8G 500G深空灰','https://img12.360buyimg.com/n7/jfs/t1/133389/8/28026/35103/6299f47aE65b351e2/e25162f3d05bc5a6.jpg','Apple MacBook Pro 14英寸 M1 Pro芯片 8G 500G深空灰笔记本电脑 MKGQ3CH/A','test',1599900,'rmb',2000,NULL,NULL,NULL,NULL,NULL,NULL),(4,1,123,'Apple MacBook Pro 14英寸 M1 Pro芯片 16G 1T深空灰','https://img12.360buyimg.com/n7/jfs/t1/145093/28/27758/83205/6171602aEddf63f15/ac38bb1342894fb5.jpg','Apple MacBook Pro 14英寸 M1 Pro芯片 16G 1T深空灰笔记本电脑 MKGQ3CH/A','ttes',1899900,'rmb',2000,NULL,NULL,NULL,NULL,NULL,NULL),(5,1,123,'Apple MacBook Pro 14英寸 M1 Pro芯片 16G 1T深空灰','https://img12.360buyimg.com/n7/jfs/t1/145093/28/27758/83205/6171602aEddf63f15/ac38bb1342894fb5.jpg','Apple MacBook Pro 14英寸 M1 Pro芯片 16G 1T深空灰笔记本电脑 MKGQ3CH/A','test',1899900,'rmb',2000,NULL,NULL,NULL,NULL,NULL,NULL),(6,1,123,'Apple MacBook Pro 14英寸 M1 Pro芯片 16G 1T深空灰','https://img12.360buyimg.com/n7/jfs/t1/145093/28/27758/83205/6171602aEddf63f15/ac38bb1342894fb5.jpg','Apple MacBook Pro 14英寸 M1 Pro芯片 16G 1T深空灰笔记本电脑 MKGQ3CH/A','test',1899900,'rmb',2000,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `pms_sku` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pms_spu`
--

DROP TABLE IF EXISTS `pms_spu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pms_spu` (
  `spu_id` bigint(20) NOT NULL COMMENT '商品id',
  `store_id` bigint(20) DEFAULT NULL COMMENT '门店id',
  `spu_name` varchar(90) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '产品名称',
  `spu_desc` varchar(900) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '商品描述',
  `unit` varchar(16) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `weight` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '商品重量',
  `publish_status` varchar(32) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '上架状态;【0 - 下架，1 - 上架】',
  `create_by` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`spu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='商品信息表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pms_spu`
--

LOCK TABLES `pms_spu` WRITE;
/*!40000 ALTER TABLE `pms_spu` DISABLE KEYS */;
INSERT INTO `pms_spu` VALUES (1,123,'测试商品spu','测试商品desc',NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `pms_spu` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pms_stock`
--

DROP TABLE IF EXISTS `pms_stock`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pms_stock` (
  `id` bigint(20) NOT NULL COMMENT 'id',
  `sku_id` bigint(20) DEFAULT NULL COMMENT 'skuId',
  `stock_count` int(11) DEFAULT NULL COMMENT '库存数量',
  `create_by` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '更新人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='商品库存表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pms_stock`
--

LOCK TABLES `pms_stock` WRITE;
/*!40000 ALTER TABLE `pms_stock` DISABLE KEYS */;
INSERT INTO `pms_stock` VALUES (123,1,111,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `pms_stock` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sms_store`
--

DROP TABLE IF EXISTS `sms_store`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sms_store` (
  `store_id` bigint(20) NOT NULL COMMENT '店铺id',
  `parent_id` bigint(20) DEFAULT NULL COMMENT '上级店铺',
  `root_store_id` bigint(20) DEFAULT NULL COMMENT '根级店铺',
  `store_name` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '店铺名称',
  `lock_status` varchar(32) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '店铺状态;正常，锁定，删除',
  `store_type` varchar(32) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '店铺类型',
  `store_role` varchar(32) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '店铺角色;单店，总部，门店、网店，仓库，合伙人',
  `join_type` varchar(32) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '加盟类型;直营，加盟',
  `online_store_open` varchar(1) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '是否开启网店',
  `created_by` varchar(32) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '创建人',
  `created_time` datetime DEFAULT NULL COMMENT '创建时间',
  `updated_by` varchar(32) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '更新人',
  `updated_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`store_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='店铺表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sms_store`
--

LOCK TABLES `sms_store` WRITE;
/*!40000 ALTER TABLE `sms_store` DISABLE KEYS */;
/*!40000 ALTER TABLE `sms_store` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ums_user_info`
--

DROP TABLE IF EXISTS `ums_user_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ums_user_info` (
  `id` bigint(20) NOT NULL COMMENT 'id',
  `user_id` bigint(20) DEFAULT NULL COMMENT '用户id',
  `regester_id` varchar(32) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '注册id',
  `sign` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '签名',
  `avatar` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '头像',
  `nickname` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '昵称',
  `country` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '国家',
  `province` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '省份',
  `ciry` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '城市',
  `county` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '县',
  `country_code` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '国家码',
  `mobile` varchar(32) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '手机号',
  `account_name` varchar(32) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '账号名',
  `password` varchar(32) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '密码',
  `gender` varchar(32) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '性别',
  `platform_type` varchar(32) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '平台信息类型;手机，微信，个人邮箱，企业邮箱',
  `account_type` varchar(32) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '账号类型;手机，微信，个人邮箱',
  `created_by` varchar(32) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '创建人',
  `created_time` datetime DEFAULT NULL COMMENT '创建时间',
  `updated_by` varchar(32) COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '更新人',
  `updated_time` datetime DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='用户信息宽表';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ums_user_info`
--

LOCK TABLES `ums_user_info` WRITE;
/*!40000 ALTER TABLE `ums_user_info` DISABLE KEYS */;
/*!40000 ALTER TABLE `ums_user_info` ENABLE KEYS */;
UNLOCK TABLES;
SET @@SESSION.SQL_LOG_BIN = @MYSQLDUMP_TEMP_LOG_BIN;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-08-06 20:53:19
